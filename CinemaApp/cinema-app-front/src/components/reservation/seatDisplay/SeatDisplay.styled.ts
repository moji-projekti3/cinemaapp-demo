import styled from 'styled-components';

const SeatGrid = styled.div<{ $numberOfColumns: number }>`
  display: grid;
  grid-template-columns: repeat(${(props) => props.$numberOfColumns}, 50px);
  grid-gap: 0px;
  justify-content: center;
`;

const SeatButton = styled.button<{
  $isReserved: boolean;
  $isSelected: boolean;
}>`
  width: 20px;
  height: 20px;
  background-color: ${({ $isReserved: isReserved, $isSelected: isSelected }) =>
    isReserved ? 'red' : isSelected ? 'green' : 'darkgrey'};
  border: none;
  cursor: ${({ $isReserved: isReserved }) =>
    isReserved ? 'not-allowed' : 'pointer'};
  display: flex;
  justify-content: center;
  align-items: center;
`;

export { SeatButton, SeatGrid };
