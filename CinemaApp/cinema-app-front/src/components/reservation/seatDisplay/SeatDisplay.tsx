import { Seat } from '../../../types/models/seat';
import { SeatButton, SeatGrid } from './SeatDisplay.styled';

interface SeatSelectionProps {
  seats: Seat[];
  selectedSeats: number[];
  onSeatSelect: (seatId: number) => void;
}

export default function SeatDisplay({
  seats,
  selectedSeats,
  onSeatSelect,
}: SeatSelectionProps) {
  const handleClick = (seatId: number) => {
    onSeatSelect(seatId);
  };

  const handleNumberOfColumns = () => {
    const uniqueColumns = new Set(seats.map((seat) => seat.column));
    return uniqueColumns.size;
  };

  const rows = seats.reduce<Record<number, Seat[]>>((acc, seat) => {
    if (!acc[seat.row]) {
      acc[seat.row] = [];
    }
    acc[seat.row].push(seat);
    return acc;
  }, {});

  const totalColumns = handleNumberOfColumns();

  return (
    <>
      {Object.keys(rows).map((row) => (
        <SeatGrid key={row} $numberOfColumns={totalColumns}>
          {rows[parseInt(row)].map((seat) => (
            <SeatButton
              key={seat.id}
              type="button"
              $isReserved={seat.isReserved}
              $isSelected={selectedSeats.includes(seat.id)}
              onClick={() => !seat.isReserved && handleClick(seat.id)}
            />
          ))}
        </SeatGrid>
      ))}
    </>
  );
}
