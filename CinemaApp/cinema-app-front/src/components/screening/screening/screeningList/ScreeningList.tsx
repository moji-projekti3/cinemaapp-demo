/* eslint-disable react/react-in-jsx-scope */
// components/screening/screeningList/ScreeningList.tsx
import { MovieRepertoireDisplay } from '../../../../types/dtos/moviesDisplayForRepertoire';
import ScreeningCard from '../screeningCard/ScreeningCard';

interface ScreeningListProps {
  movieScreenings: MovieRepertoireDisplay[];
  onScreeningClick: (screeningId: number) => void;
}

export default function ScreeningList({
  movieScreenings,
  onScreeningClick,
}: ScreeningListProps) {
  return (
    <ul>
      {movieScreenings.map((movie) => (
        <li key={movie.id}>
          <ScreeningCard
            posterImage={movie.posterImage}
            movieName={movie.name}
            originalName={movie.originalName}
            genres={movie.genres}
            duration={movie.duration}
            movieScreenings={movie.movieScreenings}
            onScreeningClick={onScreeningClick}
          />
        </li>
      ))}
    </ul>
  );
}
