/* eslint-disable react/react-in-jsx-scope */
// components/reservation/screening/ScreeningCard.tsx
import { format } from 'date-fns';
import { ScreeningRepertoireDTO } from '../../../../types/dtos/moviesDisplayForRepertoire';
import { GenreList, StyledSection } from './ScreeningCard.styled';

interface ScreeningCardProps {
  movieName: string;
  originalName: string;
  posterImage: string;
  duration: number;
  genres: string[];
  movieScreenings: ScreeningRepertoireDTO[];
  onScreeningClick: (screeningId: number) => void;
}

export default function ScreeningCard({
  movieName,
  originalName,
  posterImage,
  movieScreenings,
  genres,
  duration,
  onScreeningClick,
}: ScreeningCardProps) {
  return (
    <StyledSection>
      <figure>
        <img src={posterImage} alt="Poster image for screeningCard" />
      </figure>
      <section>
        <h4>{movieName}</h4>
        <p>{originalName}</p>
        <p>{duration}min</p>
        <GenreList>
          {genres.map((genre) => (
            <li key={genre}>{genre}</li>
          ))}
        </GenreList>
      </section>
      <div>
        <p>Rating of movie</p>
        <ul>
          {movieScreenings.map((screening) => (
            <li
              key={screening.id}
              onClick={() => onScreeningClick(screening.id)}
            >
              {format(new Date(screening.date), 'HH:mm')}
            </li>
          ))}
        </ul>
      </div>
    </StyledSection>
  );
}
