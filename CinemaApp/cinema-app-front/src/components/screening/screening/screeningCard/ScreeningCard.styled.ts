import styled from 'styled-components';

export const GenreList = styled.ul`
  li {
    font-size: 0.6rem;
    color: #555;
    background: #f4f4f4;
    padding: 1px 9px;
    border-radius: 8px;
  }
`;

export const StyledSection = styled.section`
  display: flex;
  flex-direction: row;
  border: 1px solid #ddd;
  background-color: white;
  width: 100%;
  max-width: 900px;
  width: 700px;
  height: 250px;
  padding: 20px;
  border-radius: 12px;
  box-shadow: 0 2px 8px rgba(0, 0, 0, 0.1);
  margin: 15px;
  transition: transform 0.2s ease;

  &:hover {
    transform: translateY(-5px);
  }

  figure {
    margin: 0;
    img {
      width: 140px;
      height: 100%; /* Full height for the image */
      border-radius: 8px;
      object-fit: cover;
    }
  }

  section {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    margin-left: 20px;
    flex: 1;
    position: relative;

    h4 {
      color: #222;
      font-size: 1.8rem;
      margin: 0 0 5px;
    }

    p {
      color: #666;
      margin: 5px 0;
    }

    ${GenreList} {
      position: absolute;
      bottom: 10px;
      left: 0;
      display: flex;
      flex-wrap: wrap;
      gap: 8px;
      list-style: none;
      padding: 0;
    }
  }

  div {
    text-align: right;
    p {
      font-weight: bold;
      color: #333;
    }

    ul {
      list-style: none;
      padding: 0;
      margin: 5px 0;
      display: flex;
      flex-wrap: wrap;
      gap: 5px;

      li {
        display: inline-block;
        cursor: pointer;
        padding: 6px 10px;
        border-radius: 16px;
        background-color: #f0f0f0;
        color: #333;
        transition: background-color 0.2s ease;

        &:hover {
          background-color: #e0e0e0;
        }
      }
    }
  }
`;
