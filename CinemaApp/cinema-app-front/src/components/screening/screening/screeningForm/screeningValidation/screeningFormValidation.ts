import { AddScreeningDTO } from '../../../../../types/dtos/screeningInputDTO';
import { validateNumberInput } from '../../../../../validation/inputValidation';

export interface ScreeningFormErrors {
  movieId: string;
  date: string;
  time: string;
  seatColumns: string;
  seatRows: string;
  ticketPrice: string;
}
export const validateScreeningForm = (
  formData: AddScreeningDTO,
  setErrors: React.Dispatch<React.SetStateAction<ScreeningFormErrors>>,
): boolean => {
  let valid = true;
  const newErrors: ScreeningFormErrors = {
    movieId: '',
    date: '',
    seatColumns: '',
    seatRows: '',
    time: '',
    ticketPrice: '',
  };
  // eslint-disable-next-line no-debugger
  debugger;
  const invalidColumns = validateNumberInput(formData.seatColumns, 1, 10);
  if (invalidColumns) {
    newErrors.seatColumns = 'Selected columns must be beetween 1 and 10';
    valid = false;
  }

  const invalidRows = validateNumberInput(formData.seatRows, 1, 10);
  if (invalidRows) {
    newErrors.seatRows = 'Selected rows must be beetween 1 and 10';
    valid = false;
  }

  const invalidTicketPrice = validateNumberInput(formData.ticketPrice, 5, 15);
  if (invalidTicketPrice) {
    newErrors.ticketPrice = 'Ticket price must be beetween 5 and 15';
    valid = false;
  }

  if (formData.date.length === 0) {
    newErrors.date = 'Please select date of screening.';
    valid = false;
  }

  if (formData.time.length === 0) {
    newErrors.time = 'Please select time of screening.';
    valid = false;
  }

  setErrors(newErrors);
  return valid;
};
