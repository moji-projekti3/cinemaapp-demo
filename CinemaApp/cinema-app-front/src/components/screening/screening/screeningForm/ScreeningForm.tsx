import { useState } from 'react';
import { AddScreeningDTO } from '../../../../types/dtos/screeningInputDTO';
import { Movie } from '../../../../types/models/movie';
import Input from '../../../shared/input/Input';
import Button from '../../../shared/button/Button';
import { FormContainer } from '../../../shared/login/LoginForm.styled';
import {
  ScreeningFormErrors,
  validateScreeningForm,
} from './screeningValidation/screeningFormValidation';
import { getCurrentUser } from '../../../../services/authService';
import UserRole from '../../../../types/enums/userRole';

interface ScreeningFormProps {
  onSubmit: (formData: AddScreeningDTO) => Promise<void>;
  screeningMovie: Movie;
}

export default function ScreeningForm({
  onSubmit,
  screeningMovie,
}: ScreeningFormProps) {
  const [addScreening, setAddScreening] = useState<AddScreeningDTO>({
    movieId: screeningMovie.id,
    date: '',
    time: '',
    ticketPrice: 0,
    seatRows: 0,
    seatColumns: 0,
  });

  const [errors, setErrors] = useState<ScreeningFormErrors>({
    movieId: '',
    date: '',
    seatColumns: '',
    seatRows: '',
    time: '',
    ticketPrice: '',
  });

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    getCurrentUser();
    const isValid = validateScreeningForm(addScreening, setErrors);
    if (isValid && UserRole.Admin) {
      const addScreeningFormData = {
        movieId: addScreening.movieId,
        date: addScreening.date,
        time: addScreening.time,
        seatRows: addScreening.seatRows,
        seatColumns: addScreening.seatColumns,
        ticketPrice: addScreening.ticketPrice,
      };
      const dateTime = new Date(`${addScreening.date}T${addScreening.time}`);
      const formData = {
        ...addScreeningFormData,
        date: dateTime.toISOString(),
      };
      onSubmit(formData);
    }
  };

  return (
    <FormContainer>
      <form onSubmit={handleSubmit}>
        <Input
          inputType="text"
          label="Movie"
          value={screeningMovie.name}
          disabled
        />
        <Input
          inputType="date"
          label="Date"
          name="date"
          value={addScreening.date}
          onChange={(e) =>
            setAddScreening({ ...addScreening, date: e.target.value })
          }
          error={errors.date}
        />
        <Input
          inputType="time"
          label="Time"
          name="time"
          value={addScreening.time}
          onChange={(e) =>
            setAddScreening({ ...addScreening, time: e.target.value })
          }
          error={errors.time}
        />
        <Input
          inputType="number"
          label="Ticket price"
          name="ticketPrice"
          value={addScreening.ticketPrice.toString()}
          onChange={(e) =>
            setAddScreening({
              ...addScreening,
              ticketPrice: +e.target.value,
            })
          }
          error={errors.ticketPrice}
        />
        <Input
          label="Seat rows"
          inputType="number"
          name="seatRows"
          value={addScreening.seatRows.toString()}
          onChange={(e) =>
            setAddScreening({
              ...addScreening,
              seatRows: +e.target.value,
            })
          }
          error={errors.seatRows}
        />
        <Input
          label="Seat columns"
          inputType="number"
          name="seatColumns"
          value={addScreening.seatColumns.toString()}
          onChange={(e) =>
            setAddScreening({
              ...addScreening,
              seatColumns: +e.target.value,
            })
          }
          error={errors.seatColumns}
        />
        <Button
          variant="secondary"
          size="small"
          label="Add screening"
          type="submit"
        />
      </form>
    </FormContainer>
  );
}
