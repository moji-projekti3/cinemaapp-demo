/* eslint-disable react/react-in-jsx-scope */
import { format } from 'date-fns';
import { useState } from 'react';
import { DateItem, DateList } from './DatePickerList.styled';

export const getNext7Days = (): Date[] => {
  const days: Date[] = [];
  for (let i = 0; i < 7; i++) {
    const date = new Date();
    date.setDate(date.getDate() + i);
    days.push(date);
  }
  return days;
};

interface DateComponentProps {
  onDateClick: (date: Date) => void;
}

export default function DatePickerList({ onDateClick }: DateComponentProps) {
  const [selectedDate, setSelectedDate] = useState<Date | null>(null);
  const dates = getNext7Days();

  const handleDateClick = (date: Date) => {
    setSelectedDate(date);
    onDateClick(date);
  };

  return (
    <DateList>
      {dates.map((date, index) => (
        <DateItem
          key={index}
          selected={selectedDate?.toDateString() === date.toDateString()}
          onClick={() => handleDateClick(date)}
        >
          {format(date, 'E dd.MM.')}
        </DateItem>
      ))}
    </DateList>
  );
}
