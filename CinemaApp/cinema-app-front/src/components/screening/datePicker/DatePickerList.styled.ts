import styled from 'styled-components';

export const DateList = styled.ul`
  display: flex;
  list-style: none;
  padding: 0;
  background-color: lemonchiffon;
  color: black;
`;

export const DateItem = styled.li<{ selected: boolean }>`
  padding: 3px;
  margin: 0px;
  cursor: pointer;
  background-color: ${({ selected }) => (selected ? 'lightgrey' : 'white')};
  border: 1px solid black;
  &:hover {
    background-color: lightgray;
  }
`;
