import React from 'react';
import { useAuth } from '../../Context/authContext/AuthContext';
import { Navigate, useLocation } from 'react-router-dom';
import UserRole from '../../types/enums/userRole';

interface AdminRouteProps {
  children: React.ReactNode;
}
export default function AdminRoute({ children }: AdminRouteProps) {
  const location = useLocation();
  const { role: getRole } = useAuth();

  if (getRole === UserRole.Admin) {
    return <>{children}</>;
  }
  return <Navigate to="/" state={{ from: location }} replace />;
}
