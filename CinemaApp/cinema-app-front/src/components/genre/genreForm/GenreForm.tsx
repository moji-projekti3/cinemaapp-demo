/* eslint-disable react/react-in-jsx-scope */
import { useState } from 'react';
import Genre from '../../../types/models/genre';
import Button from '../../shared/button/Button';
import Input from '../../shared/input/Input';
import { StyledDiv } from './GenreForm.styled';
import { useAuth } from '../../../Context/authContext/AuthContext';
import UserRole from '../../../types/enums/userRole';

export interface GenreFormData {
  name: string;
}

interface GenreFormProps {
  onSubmit: (data: GenreFormData) => Promise<void>;
  genre?: Genre | null;
}

export default function GenreForm({ onSubmit, genre }: GenreFormProps) {
  const { role } = useAuth();
  const [formData, setFormData] = useState<GenreFormData>({
    name: genre ? genre.name : '',
  });

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target;
    setFormData({ name: value });
  };

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    if (role === UserRole.Admin) {
      await onSubmit(formData);
    } else {
      console.error('User is not an admin');
    }
  };

  return (
    <StyledDiv>
      <form onSubmit={handleSubmit}>
        <Input
          label="Genre Name:"
          inputType="text"
          placeholder="Enter genre name"
          value={formData.name}
          onChange={handleInputChange}
          required
        />
        <Button
          type="submit"
          label={genre ? 'Update genre' : 'Add genre'}
          size="small"
          variant="primary"
          onClickHandler={() => handleSubmit}
        />
      </form>
    </StyledDiv>
  );
}
