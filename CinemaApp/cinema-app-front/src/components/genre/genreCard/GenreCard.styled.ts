import styled from 'styled-components';

export const StyledGenreCard = styled.section`
  display: inline-flex;
  flex-direction: column;
  justify-content: space-evenly;
  align-items: center;
  cursor: pointer;
  width: 200px;
  height: 100px;
  box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
  border: 1px solid #ddd;
  border-radius: 8px;
  overflow: hidden;
  background-color: lemonchiffon;
  button {
    justify-content: center;
    margin: 0;
  }
  h2 {
    font-size: 1.1rem;
    text-align: center;
    margin: 0px;
    color: black;
  }
`;
