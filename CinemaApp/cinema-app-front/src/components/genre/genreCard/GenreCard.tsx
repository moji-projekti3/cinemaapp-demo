/* eslint-disable react/react-in-jsx-scope */
import Genre from '../../../types/models/genre';
import Button from '../../shared/button/Button';
import { StyledGenreCard } from './GenreCard.styled';

interface GenreCardProps {
  genre: Genre;
  onEdit: (genre: Genre) => void;
  onDelete: (genre: Genre) => void;
}

export default function GenreCard({ genre, onEdit, onDelete }: GenreCardProps) {
  const handleEdit = (e: React.MouseEvent) => {
    e.stopPropagation();
    onEdit(genre);
  };
  const handleDelete = (e: React.MouseEvent) => {
    e.stopPropagation();
    onDelete(genre);
  };

  return (
    <StyledGenreCard onClick={handleEdit}>
      <h2>{genre.name}</h2>
      <Button
        type="button"
        label="Delete"
        size="small"
        variant="secondary"
        onClickHandler={handleDelete}
      />
    </StyledGenreCard>
  );
}
