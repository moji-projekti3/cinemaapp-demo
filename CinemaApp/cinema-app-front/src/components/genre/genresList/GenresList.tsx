import Genre from '../../../types/models/genre';
import GenreCard from '../genreCard/GenreCard';
import { StyledGenresList } from './GenresList.styled';

interface GenresListProps {
  genres: Genre[];
  onEdit: (genre: Genre) => void;
  onDelete: (genre: Genre) => void;
}
export default function GenresList({
  genres,
  onEdit,
  onDelete,
}: GenresListProps) {
  return (
    <StyledGenresList>
      {genres.map((genre) => (
        <GenreCard
          key={genre.id}
          genre={genre}
          onEdit={onEdit}
          onDelete={onDelete}
        />
      ))}
    </StyledGenresList>
  );
}
