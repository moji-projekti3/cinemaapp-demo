import styled from 'styled-components';

export const StyledGenresList = styled.article`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-around;
  gap: 2rem;
  background-color: lemonchiffon;
  padding: 1rem;
  border-radius: 10px;
  box-shadow: 0 4px 8px rgba(0, 0, 0, 0.4);
  margin: 1rem;
  transition: transform 0.6s;

  section:hover {
    transform: scale(1.1);
  }

  h3 {
    text-align: center;
    font-size: 1.8rem;
    margin-bottom: 0.5rem;
    color: black;
  }

  ul {
    display: inline-flex;
    flex-direction: row;
    gap: 3rem;
    padding: 0;
    list-style-type: none;
    margin: 0;
  }

  li {
    font-size: 1rem;
    color: black;
    background-color: snow;
    padding: 0.5rem 1rem;
    border-radius: 8px;
    box-shadow: 0 4px 8px rgba(0, 0, 0, 0.4);
    list-style-type: none;
  }
`;
