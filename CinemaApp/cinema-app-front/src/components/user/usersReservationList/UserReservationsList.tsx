/* eslint-disable react/react-in-jsx-scope */
import { UserReservationsDisplay } from '../../../types/dtos/userReservationsDisplayDTO';
import ReservationCard from '../usersReservationsCard/UserReservationsCard';

interface ReservationListProps {
  screeningReservations: UserReservationsDisplay[];
  onCancelReservation: (reservationId: number) => void;
  isPastReservation: boolean;
}
export default function ReservationsList({
  screeningReservations,
  onCancelReservation,
  isPastReservation,
}: ReservationListProps) {
  return (
    <ul>
      {screeningReservations.map((reservation) => (
        <li key={reservation.id}>
          <ReservationCard
            reservationId={reservation.id}
            movieName={reservation.movieScreening.movieName}
            moviePosterImage={reservation.movieScreening.moviePosterImage}
            date={reservation.movieScreening.date}
            time={reservation.movieScreening.time}
            totalPrice={reservation.totalPrice}
            reservedSeats={reservation.reservedSeats}
            onCancelReservation={onCancelReservation}
            isPastReservation={isPastReservation}
          />
        </li>
      ))}
    </ul>
  );
}
