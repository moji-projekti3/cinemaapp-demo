/* eslint-disable react/react-in-jsx-scope */
import { format } from 'date-fns';
import {
  RatingAndPrice,
  SeatsInfo,
  StyledSection,
} from './UserReservationCard.styled';
import { Seat } from '../../../types/models/seat';
import Button from '../../shared/button/Button';

interface ReservationCardProps {
  reservationId: number;
  moviePosterImage: string;
  movieName: string;
  date: string;
  time: string;
  totalPrice: number;
  reservedSeats: Seat[];
  onCancelReservation: (reservationId: number) => void;
  isPastReservation: boolean;
}

export default function ReservationCard({
  reservationId,
  moviePosterImage,
  movieName,
  date,
  reservedSeats,
  totalPrice,
  onCancelReservation,
  isPastReservation,
}: ReservationCardProps) {
  return (
    <StyledSection>
      <figure>
        <img src={moviePosterImage} alt="Poster image for reservationCard" />
      </figure>
      <section>
        <h4>{movieName}</h4>
        <p>Date: {format(new Date(date), 'dd.MM.yyyy')}</p>
        <p>Time: {format(new Date(date), 'HH:mm')}</p>
      </section>
      <SeatsInfo>
        <p>No. of tickets: {reservedSeats.length}</p>
        <ul>
          Seats numbers:
          {reservedSeats.map((seat) => (
            <li key={seat.id}>{seat.id}</li>
          ))}
        </ul>
      </SeatsInfo>
      <RatingAndPrice>
        <p>Rating</p>
        <p>Total Price {totalPrice.toFixed(2)}$</p>
      </RatingAndPrice>
      {isPastReservation ? null : (
        <Button
          label="Cancel reservation"
          size="medium"
          variant="secondary"
          onClickHandler={() => onCancelReservation(reservationId)}
        />
      )}
    </StyledSection>
  );
}
