import styled from 'styled-components';

export const StyledSection = styled.section`
  display: flex;
  border: 2px solid black;
  background-color: #f4f4f4;
  width: 50rem;
  padding: 20px;
  border-radius: 8px;
  margin: 10px;
  position: relative;
  figure {
    margin: 0;
    img {
      width: 100px;
      height: 150px;
    }
  }
  h4 {
    color: black;
    position: relative;
    margin-top: 0;
  }
  section {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    margin-left: 20px;
  }

  p {
    font-size: 1rem;
    color: black;
    margin-top: 0;
    padding-top: 2px;
  }
  button {
    position: absolute;
    bottom: 10px;
    right: 10px;
  }
`;
export const SeatsInfo = styled.div`
  display: flex;
  padding: 20px;
  flex-direction: column;
  justify-content: end;
  align-items: center;
  gap: 10px;
  ul {
    padding-left: 90px;
    color: black;
    li {
      align-items: start;
    }
  }
  p {
    margin: 0;
    padding-left: 90px;
    padding-top: 2px;
    color: black;
  }
`;

export const RatingAndPrice = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding: 40px;
  color: black;
`;
