import React from 'react';
import { StyledLoader } from './Loader.styled';

type Loader = {
  message: string | undefined;
};

export const Loader: React.FC<Loader> = ({ message }) => {
  return (
    <StyledLoader>
      <span className="spinner"></span>
      <span className="loader-message">{message}</span>
    </StyledLoader>
  );
};
