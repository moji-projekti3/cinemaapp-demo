import styled from 'styled-components';

export const StyledFooter = styled.footer`
  overflow: hidden;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  background: #2a2a2a;
  color: #f5f5f5;
  font-family: 'Montserrat', sans-serif;
  font-size: 0.8rem;
  padding: 0.1rem;
  box-shadow: 0 -2px 4px rgba(0, 0, 0, 0.4); /* Subtle shadow for depth */
  border-top: 1px solid #444;

  ul {
    display: flex;
    justify-content: center;
    gap: 15px;
    padding: 0;
    list-style: none;
  }

  p {
    margin: 10px 0 0 0;
    text-align: center;
  }

  a {
    color: #f5f5f5;
    text-decoration: none;
    transition: color 0.2s ease;

    &:hover {
      color: #1e90ff; /* Subtle blue on hover */
    }
  }
`;
