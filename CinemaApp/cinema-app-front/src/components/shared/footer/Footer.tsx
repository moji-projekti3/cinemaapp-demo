import { SocialIcon } from 'react-social-icons';
import { StyledFooter } from './Footer.styled';

type FooterProps = {
  label: string;
  devName: string;
};

const socialMediaLinks = [
  {
    url: 'https://instagram.com',
    label: 'Instagram',
    style: { height: 25, width: 25, marginRight: 3 },
  },
  {
    url: 'https://facebook.com',
    label: 'Facebook',
    style: { height: 25, width: 25, marginRight: 3 },
  },
  {
    url: 'https://youtube.com',
    label: 'YouTube',
    style: { height: 25, width: 25, marginRight: 3 },
  },
  {
    url: 'https://tiktok.com',
    label: 'TikTok',
    style: { height: 25, width: 25, marginRight: 3 },
  },
  {
    url: 'https://x.com',
    label: 'X',
    style: { height: 25, width: 25, marginRight: 3 },
  },
];

const Footer = ({ label, devName }: FooterProps) => {
  return (
    <StyledFooter>
      <p>{label}</p>
      <p>{devName}</p>
      <ul>
        {socialMediaLinks.map((link) => (
          <SocialIcon key={link.url} url={link.url} style={link.style} />
        ))}
      </ul>
    </StyledFooter>
  );
};

export default Footer;
