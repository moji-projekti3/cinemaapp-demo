import React from 'react';
import {
  StyledDiv,
  StyledInput,
  StyledLabel,
  StyledError,
} from './Input.styled';

interface InputProps {
  label: string;
  inputType: string;
  name?: string;
  placeholder?: string;
  value?: string | string[];
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  required?: boolean;
  file?: File;
  accept?: string;
  disabled?: boolean;
  error?: string | boolean;
}

export default function Input({
  label,
  inputType,
  onChange,
  error,
  ...props
}: InputProps) {
  return (
    <StyledDiv>
      <StyledLabel>{label}</StyledLabel>
      <StyledInput
        type={inputType}
        onChange={onChange}
        $error={!!error}
        {...props}
      />
      {error && <StyledError>{error}</StyledError>}
    </StyledDiv>
  );
}
