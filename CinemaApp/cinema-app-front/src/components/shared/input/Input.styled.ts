import styled from 'styled-components';

export const StyledDiv = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: start;
  padding: 1rem;
  padding-right: 5rem;
  background-color: #f5f5f5;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
`;

export const StyledInput = styled.input<{ $error?: boolean }>`
  width: 100%;
  padding: 0.5rem;
  background: #ecf0f3;
  padding: 0px;
  padding-left: 10px;
  height: 20px;
  font-size: 14px;
  border-radius: 50px;
  border: 1px solid ${(props) => (props.$error ? '#ff0000' : '#ccc')};
  border-radius: 8px;
  box-shadow: ${(props) =>
    props.$error ? '0 0 5px rgba(255, 0, 0, 0.5)' : 'none'};
  transition:
    border-color 0.3s,
    box-shadow 0.3s;

  &:focus {
    border-color: ${(props) => (props.$error ? '#ff0000' : '#66afe9')};
    box-shadow: ${(props) =>
      props.$error
        ? '0 0 5px rgba(255, 0, 0, 0.5)'
        : '0 0 5px rgba(102, 175, 233, 0.5)'};
    outline: none;
  }
`;

export const StyledLabel = styled.label`
  color: black;
  border: none;
  margin-right: 0.5rem;
`;
export const StyledError = styled.div`
  color: #d63301;
  background-color: transparent;
`;
