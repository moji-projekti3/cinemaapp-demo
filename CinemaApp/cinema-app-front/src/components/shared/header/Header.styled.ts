import styled from 'styled-components';

export const StyledHeader = styled.header`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 1rem 2rem;
  background: #2a2a2a; /* Clean and consistent background */
  color: #ddd;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.2);
  border-bottom: 1px solid #444;
  border-radius: 12px 12px 0 0; /* Optional for rounded top corners */

  button {
    display: inline-flex;
    align-items: center;
    justify-content: center;
    background: #444;
    color: #ddd;
    font-size: 0.9rem;
    padding: 6px 12px;
    margin-left: 20px;
    border: 1px solid #666;
    border-radius: 4px;
    cursor: pointer;
    transition:
      background-color 0.3s ease,
      color 0.2s ease;

    &:hover {
      background-color: #555;
      color: #fff;
      border-color: #888;
    }

    &:active {
      background-color: #666;
      transform: scale(0.95);
    }
  }

  h1 {
    font-size: 1.8rem;
    font-weight: 700;
    margin: 0;
    color: #f5f5f5;
    text-shadow: 0 1px 3px rgba(0, 0, 0, 0.3);
  }
`;

export const Navigation = styled.nav`
  align-items: center;
  margin-left: auto;

  ul {
    display: inline-flex;
    list-style-type: none;
    padding: 0;
    margin: 0;

    a {
      font-size: 1rem;
      margin-left: 20px;
      text-decoration: none;
      color: #ddd;
      transition: color 0.2s ease;

      &:hover {
        color: #f5f5f5;
      }

      &:active {
        color: #4682b4;
      }
    }
  }
`;
