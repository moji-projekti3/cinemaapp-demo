/* eslint-disable react/react-in-jsx-scope */
import { ReactNode } from 'react';
import { Navigation, StyledHeader } from './Header.styled';
import { LinkProps } from 'react-router-dom';
import { LinkItem } from '../../../services/config/linksConfig';

interface HeaderProps {
  appTitle: string;
  LinkComponent: React.ComponentType<LinkProps>;
  links: LinkItem[];
  children?: ReactNode;
}

export default function Header({
  appTitle,
  LinkComponent,
  links,
  children,
}: HeaderProps) {
  return (
    <StyledHeader>
      <h1>{appTitle}</h1>
      <Navigation>
        <ul>
          {links.map((link) => (
            <li key={link.linkText}>
              <LinkComponent to={link.href}>{link.linkText}</LinkComponent>
            </li>
          ))}
        </ul>
      </Navigation>
      {children}
    </StyledHeader>
  );
}
