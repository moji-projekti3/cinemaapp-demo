// src/components/pageLink/PageLink.tsx

import styled from 'styled-components';

const StyledPageLink = styled.a<{ active?: boolean; disabled?: boolean }>`
  position: relative;
  display: inline-flex;
  border: 1px solid #dee2e6;
  background-color: #ffffff;
  padding: 10px 15px;
  color: ${({ active }) => (active ? 'wheat' : 'black')};
  font-size: 13px;
  font-weight: 600;
  text-decoration: none;
  transition:
    color 0.15s ease-in-out,
    background-color 0.15s ease-in-out,
    border-color 0.15s ease-in-out;
  cursor: pointer;
  pointer-events: ${({ disabled }) => (disabled ? 'none' : 'auto')};

  &:first-child {
    border-top-left-radius: 5px;
    border-bottom-left-radius: 5px;
  }

  &:last-child {
    border-top-right-radius: 5px;
    border-bottom-right-radius: 5px;
  }

  &:not(:first-child) {
    margin-left: -1px;
  }

  &:hover,
  &:focus {
    color: ${({ active }) => (active ? '#ffffff' : '#0a58ca')};
    background-color: #e9ecef;
  }

  &:focus {
    z-index: 3;
  }

  &.active {
    z-index: 2;
    color: black;
    background-color: #e9ecef;
  }

  &.disabled {
    color: #0d6efd;
    pointer-events: none;
  }
`;

export default StyledPageLink;
