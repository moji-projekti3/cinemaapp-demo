/* eslint-disable react/react-in-jsx-scope */
import { ReactNode } from 'react';
import { StyledButton } from './Button.styled';

interface ButtonProps {
  label: string;
  onClickHandler?: (e: React.MouseEvent) => void;
  type?: 'button' | 'submit' | 'reset';
  variant?: 'primary' | 'secondary' | 'info';
  size?: 'small' | 'medium' | 'large';
  disabled?: boolean;
  isSelected?: boolean;
  children?: ReactNode;
}

export default function Button({
  label,
  onClickHandler,
  type = 'button',
  variant,
  size,
  children,
  isSelected,
}: ButtonProps) {
  return (
    <StyledButton
      $variant={variant}
      $size={size}
      $isSelected={isSelected}
      type={type}
      onClick={onClickHandler}
    >
      {label}
      {children}
    </StyledButton>
  );
}
