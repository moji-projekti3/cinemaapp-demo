import styled from 'styled-components';

export const StyledButton = styled.button<{
  $variant?: 'primary' | 'secondary' | 'info';
  $size?: 'small' | 'medium' | 'large';
  $isSelected?: boolean;
}>`
  background: ${({ $variant }) => backgroundColors[$variant || 'primary']};
  color: ${({ $variant }) => ($variant === 'info' ? '#333' : 'white')};
  font-size: ${({ $size }) => buttonFontSizes[$size || 'medium']};
  padding: ${({ $size }) => buttonPaddings[$size || 'medium']};
  border: ${({ $variant }) =>
    $variant === 'info' ? '1px solid #ccc' : 'none'};
  border-radius: ${({ $variant }) => ($variant === 'info' ? '9px' : '3px')};
  // border-radius: 18px;
  cursor: pointer;
  transition: all 0.2s ease;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);

  &:hover {
    background: ${({ $variant }) =>
      hoverBackgroundColors[$variant || 'primary']};
    box-shadow: 0 4px 8px rgba(0, 0, 0, 0.15);
  }

  &:active {
    transform: scale(0.95);
  }

  &:disabled {
    background: #cccccc;
    cursor: not-allowed;
    box-shadow: none;
  }
`;

const backgroundColors = {
  primary: '#007bff',
  secondary: '#dc3545',
  info: '#f8f9fa',
};

const hoverBackgroundColors = {
  primary: '#0056b3',
  secondary: '#c82333',
  info: '#e2e6ea',
};

const buttonFontSizes = {
  small: '12px',
  medium: '14px',
  large: '16px',
};

const buttonPaddings = {
  small: '6px 12px',
  medium: '8px 16px',
  large: '10px 20px',
};
