import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import Button from '../button/Button';
import Input from '../input/Input';
import { FormContainer } from './LoginForm.styled';
import {
  validateLoginForm,
  LoginFormErrors,
} from './loginValidation/loginFormValidation';
import { Login } from '../../../types/models/login';

interface LoginFormProps {
  onSubmit: (data: Login) => Promise<void>;
}

export default function LoginForm({ onSubmit }: LoginFormProps) {
  const [login, setLogin] = useState<Login>({
    email: '',
    password: '',
  });

  const [errors, setErrors] = useState<LoginFormErrors>({
    email: '',
    password: '',
  });

  const [submitted, setSubmitted] = useState(false);
  const navigate = useNavigate();

  const handleUserInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.currentTarget;
    setLogin((prevState: Login) => ({
      ...prevState,
      [name]: value,
    }));
    setErrors((prevErrors) => ({
      ...prevErrors,
      [name]: '',
    }));
  };

  const handleLogin = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    setSubmitted(true);
    const isValid = validateLoginForm(login, setErrors);
    if (isValid) {
      const loginInputData = {
        email: login.email,
        password: login.password,
      };
      await onSubmit(loginInputData);
      navigate('/');
    }
  };

  return (
    <FormContainer>
      <form onSubmit={handleLogin}>
        <Input
          inputType="text"
          label="Email: "
          placeholder="Enter email address"
          name="email"
          value={login.email}
          onChange={handleUserInput}
          error={submitted && errors.email}
        />
        <Input
          inputType="password"
          label="Password: "
          placeholder="Enter password"
          name="password"
          value={login.password}
          onChange={handleUserInput}
          error={submitted && errors.password}
        />
        <Button variant="secondary" size="medium" type="submit" label="Login" />
      </form>
    </FormContainer>
  );
}
