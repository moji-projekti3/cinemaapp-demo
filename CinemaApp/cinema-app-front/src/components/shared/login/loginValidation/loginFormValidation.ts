import { Login } from '../../../../types/models/login';
import {
  validateEmail,
  validatePassword,
} from '../../../../validation/inputValidation';

export interface LoginFormErrors {
  email: string;
  password: string;
}
export const validateLoginForm = (
  formData: Login,
  setErrors: React.Dispatch<React.SetStateAction<LoginFormErrors>>,
): boolean => {
  let valid = true;
  const newErrors: LoginFormErrors = {
    email: '',
    password: '',
  };

  const invalidEmail = validateEmail(formData.email);
  if (invalidEmail) {
    newErrors.email = 'Please enter valid email address.';
    valid = false;
  }

  const invalidPassword = validatePassword(formData.password);
  if (invalidPassword) {
    newErrors.password = 'Please enter valid password.';
    valid = false;
  }

  setErrors(newErrors);
  return valid;
};
