import styled from 'styled-components';

export const FormContainer = styled.div`
  align-self: center;
  position: relative;
  width: 100%; 
  max-width: 450px; 
  height: auto;
  border-radius: 20px;
  padding: 40px;
  box-sizing: border-box;
  background: #ecf0f3;
  box-shadow:
    6px 6px 10px #cbced1,
    -6px -8px 10px white;
  overflow-y: auto; 

  form {
    align-self: center;
    border-radius: 8px;
    label,
    input,
    button {
      display: block;
      width: 100%;
      padding: 0;
      border: none;
      outline: none;
      box-sizing: border-box;
    }
    button {
      margin: 0;
    }
    label {
      margin-bottom: 4px;
    }

    label:nth-of-type(2) {
      margin-top: 12px;
    }

    input::placeholder {
      color: gray;
    }

    input {
      background: #ecf0f3;
      padding: 0px;
      padding-left: 10px;
      height: 50px;
      font-size: 14px;
      border-radius: 50px;
      box-shadow:
        inset 10px 6px 6px #cbced1,
        inset -6px -6px 6px white;
    }

    button {
      color: black;
      margin-top: 20px;
      background: #ecf0f3;
      height: 40px;
      border-radius: 20px;
      cursor: pointer;
      font-weight: 900;
      box-shadow:
        6px 6px 6px #cbced1,
        -6px -6px 6px white;
      transition: 0.5s;
    }

    button:hover {
      box-shadow: none;
    }
  }
`;
