import { Register } from '../../../../types/models/register';
import {
  validateEmail,
  validatePassword,
  validateTextInput,
} from '../../../../validation/inputValidation';

export interface registerFormErrors {
  name: string;
  email: string;
  password: string;
  dateOfBirth: string;
}
export const validateRegisterForm = (
  formData: Register,
  setErrors: React.Dispatch<React.SetStateAction<registerFormErrors>>,
): boolean => {
  let valid = true;
  const newErrors: registerFormErrors = {
    name: '',
    email: '',
    password: '',
    dateOfBirth: '',
  };

  const invalidName = validateTextInput(formData.name, 2, 45);
  if (invalidName) {
    newErrors.name = 'Name must be at least 2 characters long.';
    valid = false;
  }

  const invalidEmail = validateEmail(formData.email);
  if (invalidEmail) {
    newErrors.email = 'Please enter valid email address.';
    valid = false;
  }

  const invalidPassword = validatePassword(formData.password);
  if (invalidPassword) {
    newErrors.password = 'Please enter valid password.';
    valid = false;
  }
  if (formData.dateOfBirth.length === 0) {
    newErrors.dateOfBirth = 'Please enter date of birth.';
    valid = false;
  }
  setErrors(newErrors);
  return valid;
};
