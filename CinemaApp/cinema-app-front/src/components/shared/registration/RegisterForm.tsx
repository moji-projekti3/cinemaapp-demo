import { useState } from 'react';
import { Register } from '../../../types/models/register';
import Button from '../button/Button';
import Input from '../input/Input';
import { FormContainer } from '../login/LoginForm.styled';
import {
  registerFormErrors,
  validateRegisterForm,
} from './registerValidation/registerFormValidation';

interface RegisterFormProps {
  onSubmit: (registerData: Register) => Promise<void>;
}

export default function RegisterForm({ onSubmit }: RegisterFormProps) {
  const [registerFormData, setRegisterFormData] = useState<Register>({
    name: '',
    email: '',
    password: '',
    dateOfBirth: '',
  });

  const [errors, setErrors] = useState<registerFormErrors>({
    name: '',
    email: '',
    password: '',
    dateOfBirth: '',
  });

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const isValid = validateRegisterForm(registerFormData, setErrors);
    if (isValid) {
      const registerInput = {
        name: registerFormData.name,
        email: registerFormData.email,
        password: registerFormData.password,
        dateOfBirth: registerFormData.dateOfBirth,
      };
      await onSubmit(registerInput);
    }
  };

  return (
    <FormContainer>
      <form onSubmit={handleSubmit}>
        <Input
          inputType="text"
          label="Name: "
          placeholder="Enter your name"
          value={registerFormData.name}
          onChange={(event) =>
            setRegisterFormData({
              ...registerFormData,
              name: event.target.value,
            })
          }
          error={errors.name}
        />
        <Input
          inputType="email"
          label="Email: "
          placeholder="Enter your email address"
          value={registerFormData.email}
          onChange={(event) =>
            setRegisterFormData({
              ...registerFormData,
              email: event.target.value,
            })
          }
          error={errors.email}
        />
        <Input
          inputType="password"
          label="Password: "
          placeholder="Enter your password"
          value={registerFormData.password}
          onChange={(event) =>
            setRegisterFormData({
              ...registerFormData,
              password: event.target.value,
            })
          }
          error={errors.password}
        />
        <Input
          inputType="date"
          label="Date of birth: "
          value={registerFormData.dateOfBirth}
          onChange={(event) =>
            setRegisterFormData({
              ...registerFormData,
              dateOfBirth: event.target.value,
            })
          }
          error={errors.dateOfBirth}
        />
        <Button
          label="Register"
          size="medium"
          type="submit"
          variant="secondary"
        />
      </form>
    </FormContainer>
  );
}
