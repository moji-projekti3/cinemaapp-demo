import styled from 'styled-components';

export const MoviesDisplayUl = styled.ul`
  display: flex;
  flex-direction: row;
  gap: 50px;
  justify-content: space-between;
  list-style-type: none;
  li {
    transition: transform 0.6s;
    border: 2px solid black;
    border-radius: 8px;
    box-shadow: 6px 5px 7px rgba(5, 5, 0, 5);
  }
  li:hover {
    transform: scale(1.1);
  }
`;
