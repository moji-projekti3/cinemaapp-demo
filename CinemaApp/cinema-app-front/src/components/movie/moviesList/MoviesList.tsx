// src/components/movie/moviesList/MoviesDisplay.tsx
import React from 'react';
import { Movie } from '../../../types/models/movie';
import MovieCard from '../movieCard/MovieCard';
import { MoviesDisplayUl } from './MoviesList.styled';

interface MovieListProps {
  movies: Movie[];
  onEdit: (movie: Movie) => void;
  onDelete: (movie: Movie) => void;
  onAddScreening: (movie: Movie) => void;
}

export default function MoviesList({
  movies,
  onDelete,
  onEdit,
  onAddScreening,
}: MovieListProps) {
  return (
    <MoviesDisplayUl>
      {movies.map((movie) => {
        return (
          <li key={movie.id}>
            <MovieCard
              onEdit={onEdit}
              onDelete={onDelete}
              movie={movie}
              onAddScreening={onAddScreening}
            />
          </li>
        );
      })}
    </MoviesDisplayUl>
  );
}
