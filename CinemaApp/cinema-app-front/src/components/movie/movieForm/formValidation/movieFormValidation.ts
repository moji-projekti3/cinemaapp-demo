import {
  validateFileInput,
  validateNumberInput,
  validateTextInput,
} from '../../../../validation/inputValidation';
import { MovieFormData } from '../MovieForm';

export interface FormErrors {
  name: string;
  originalName: string;
  duration: string;
  genreIds: string;
  posterImage: string;
}
export const validateForm = (
  formData: MovieFormData,
  setErrors: React.Dispatch<React.SetStateAction<FormErrors>>,
): boolean => {
  let valid = true;
  const newErrors: FormErrors = {
    name: '',
    originalName: '',
    duration: '',
    genreIds: '',
    posterImage: '',
  };

  const nameError = validateTextInput(formData.name, 2, 45);
  if (nameError) {
    newErrors.name = 'Name must have at least 2 characters.';
    valid = false;
  }

  const originalNameError = validateTextInput(formData.originalName, 2, 45);
  if (originalNameError) {
    newErrors.originalName = 'Original name must have at least 2 characters.';
    valid = false;
  }

  const durationError = validateNumberInput(formData.duration, 30, 300);
  if (durationError) {
    newErrors.duration = 'Duration must be minimum 30min and maximum 300min.';
    valid = false;
  }

  if (formData.genreIds.length === 0) {
    newErrors.genreIds = 'Please select at least one genre.';
    valid = false;
  }

  const posterImageError = validateFileInput(
    formData.posterImage as File,
    false,
    'image/',
  );
  if (!posterImageError) {
    newErrors.posterImage = 'Please choose image type of file.';
    valid = false;
  }

  setErrors(newErrors);
  return valid;
};
