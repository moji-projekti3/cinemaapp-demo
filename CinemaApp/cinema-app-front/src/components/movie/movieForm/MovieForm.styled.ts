import styled from 'styled-components';

export const StyledDiv = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: auto;
  padding: 20px;
  background: linear-gradient(135deg, #2e2e2e, #1c1c1c);

  form {
    width: 100%;
    max-width: 800px;
    background: #27293d;
    border-radius: 12px;
    box-shadow: 0 4px 8px rgba(0, 0, 0, 0.5);
    padding: 2rem;
    color: #f5f5f5;

    h2 {
      font-size: 1.8rem;
      color: #ff5e57;
      margin-bottom: 1.5rem;
      text-align: center;
    }

    label {
      display: block;
      font-size: 1rem;
      font-weight: bold;
      color: #d1d1d1;
      margin-bottom: 8px;
    }

    input,
    select {
      width: 100%;
      padding: 12px 15px;
      margin-bottom: 1.5rem;
      border: 1px solid #555;
      border-radius: 8px;
      font-size: 1rem;
      background: #35364d;
      color: #f5f5f5;
      transition:
        border-color 0.3s ease,
        box-shadow 0.2s ease;

      &:focus {
        border-color: #ff5e57;
        box-shadow: 0 0 8px rgba(255, 94, 87, 0.8);
        outline: none;
      }

      &::placeholder {
        color: #bbb;
      }
    }

    button {
      width: 100%;
      padding: 14px 20px;
      font-size: 1.1rem;
      font-weight: bold;
      color: white;
      background: #ff5e57;
      border: none;
      border-radius: 8px;
      cursor: pointer;
      transition:
        background 0.3s ease,
        transform 0.1s ease;

      &:hover {
        background: #e04c46;
      }

      &:active {
        background: #c03d3b;
        transform: scale(0.97);
      }
    }

    .error {
      color: #dc3545;
      font-size: 0.8rem;
      margin-top: -10px;
      margin-bottom: 10px;
    }
  }
`;

export const StyledSelect = styled.select`
  border: 1px solid #555;
  border-radius: 8px;
  padding: 12px 15px;
  font-size: 1rem;
  background: #35364d;
  color: #f5f5f5;
  transition: border-color 0.3s ease;

  &:focus {
    border-color: #ff5e57;
    outline: none;
  }
`;

export const StyledError = styled.span`
  color: #dc3545;
  font-size: 0.8rem;
  margin-top: -10px;
  margin-bottom: 10px;
`;
