import React, { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import genreService from '../../../services/genreService';
import Genre from '../../../types/models/genre';
import { Movie } from '../../../types/models/movie';
import Button from '../../shared/button/Button';
import Input from '../../shared/input/Input';
import { StyledError, StyledLabel } from '../../shared/input/Input.styled';
import { StyledDiv, StyledSelect } from './MovieForm.styled';
import { FormErrors, validateForm } from './formValidation/movieFormValidation';

export interface MovieFormData {
  name: string;
  originalName: string;
  duration: number;
  genreIds: number[];
  posterImage?: File | string;
}

interface MovieFormProps {
  onSubmit: (formData: FormData) => Promise<void>;
  movie?: Movie | null;
}

export default function MovieForm({ onSubmit, movie }: MovieFormProps) {
  const [formData, setFormData] = useState<MovieFormData>({
    name: movie?.name || '',
    originalName: movie?.originalName || '',
    duration: movie?.duration || 0,
    genreIds: movie?.genres.map((genre) => genre.id) || [],
    posterImage: movie?.posterImage,
  });

  const [genres, setGenres] = useState<Genre[]>([]);
  const [errors, setErrors] = useState<FormErrors>({
    name: '',
    originalName: '',
    duration: '',
    genreIds: '',
    posterImage: '',
  });

  useEffect(() => {
    fetchGenres();
  }, []);

  async function fetchGenres() {
    try {
      const response = await genreService.getGenres();
      const pagedGenresResponse = response.data;
      setGenres(pagedGenresResponse.data);
    } catch (error) {
      toast.error('Error occurred while fetching genres!');
    }
  }

  const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const files = event.target.files;
    if (files && files.length > 0) {
      setFormData({ ...formData, posterImage: files[0] });
    }
  };

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const isValid = validateForm(formData, setErrors);
    if (isValid) {
      const movieDetails = {
        originalName: formData.originalName,
        name: formData.name,
        duration: formData.duration,
        genreIds: formData.genreIds,
      };

      const submitFormData = new FormData();
      submitFormData.append('movie', JSON.stringify(movieDetails));
      if (formData.posterImage instanceof File) {
        submitFormData.append('posterImage', formData.posterImage);
      }
      onSubmit(submitFormData);
    }
  };

  return (
    <StyledDiv>
      <form onSubmit={handleSubmit}>
        <Input
          label="Original Name"
          inputType="text"
          placeholder="Enter original name"
          value={formData.originalName}
          onChange={(e) =>
            setFormData({ ...formData, originalName: e.target.value })
          }
          error={errors.originalName}
        />

        <Input
          label="Name"
          inputType="text"
          placeholder="Enter name"
          value={formData.name}
          onChange={(e) => setFormData({ ...formData, name: e.target.value })}
          error={errors.name}
        />

        <Input
          label="Duration"
          inputType="number"
          placeholder="Enter duration"
          value={formData.duration.toString()}
          onChange={(e) =>
            setFormData({ ...formData, duration: +e.target.value })
          }
          error={errors.duration}
        />

        <StyledLabel htmlFor="genres">Genres</StyledLabel>
        <StyledSelect
          id="genres"
          value={formData.genreIds.map((id) => id.toString())}
          onChange={(e) =>
            setFormData({
              ...formData,
              genreIds: Array.from(e.target.selectedOptions, (option) =>
                parseInt(option.value),
              ),
            })
          }
          multiple
        >
          {genres.map((genre) => (
            <option key={genre.id} value={genre.id}>
              {genre.name}
            </option>
          ))}
        </StyledSelect>
        {errors.genreIds && <StyledError>{errors.genreIds}</StyledError>}
        <Input
          label="Poster Image"
          inputType="file"
          onChange={handleFileChange}
          accept="image/*"
          error={errors.posterImage}
        />
        <Button
          type="submit"
          label={movie ? 'Update movie' : 'Add movie'}
          size="small"
          variant="primary"
          onClickHandler={() => handleSubmit}
        />
      </form>
    </StyledDiv>
  );
}
