import styled from 'styled-components';

const Card = styled.article`
  border: 1px solid #ddd;
  border-radius: 8px;
  background-color: black;
  width: 200px;
  height: 370px;
  box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
  display: inline-flex;
  flex-direction: column;
  justify-content: space-between;
  cursor: pointer;
  transition: transform 0.3s ease;
  img {
    width: 100%;
    height: 220px;
  }
`;

const MovieInfo = styled.section`
  padding: 10px;
  background-color: inherit;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;

  h3 {
    font-size: 0.8rem;
    margin: 0;
    color: wheat;
  }

  p {
    font-size: 0.6rem;
    margin-top: 0.2rem;
    margin-bottom: 0;
    color: wheat;
  }

  ul {
    font-size: 0.7rem;
    background-color: inherit;
    color: wheat;
    margin-top: 0.2rem;
    gap: 3px;
    display: flex;
    flex-direction: row;
    list-style-type: none;
    padding: 0;
  }

  li {
    margin: 0;
    text-align: left;
    padding-left: 0;
  }
  button {
    margin-top: 0.5rem;
    margin-bottom: 0;
    align-self: center;
    width: 100px;
    height: 25px;
    font-size: 0.6rem;
  }
`;

export default {
  Card,
  MovieInfo,
};
