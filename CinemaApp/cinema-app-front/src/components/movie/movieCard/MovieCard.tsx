/* eslint-disable react/react-in-jsx-scope */
import { useAuth } from '../../../Context/authContext/AuthContext';
import UserRole from '../../../types/enums/userRole';
import { Movie } from '../../../types/models/movie';
import Button from '../../shared/button/Button';
import StyledMovieCard from './MovieCard.styled';

const { Card, MovieInfo } = StyledMovieCard;

interface MovieCardProps {
  movie: Movie;
  onDelete: (movie: Movie) => void;
  onEdit: (movie: Movie) => void;
  onAddScreening: (movie: Movie) => void;
}

export default function MovieCard({
  movie,
  onDelete,
  onEdit,
  onAddScreening,
}: MovieCardProps) {
  const { role: getRole } = useAuth();

  const handleEdit = (e: React.MouseEvent) => {
    e.stopPropagation();
    if (getRole === UserRole.Admin) {
      onEdit(movie);
    }
  };

  const handleDelete = (e: React.MouseEvent) => {
    e.stopPropagation();
    onDelete(movie);
  };

  const handleAddScreening = (e: React.MouseEvent) => {
    e.stopPropagation();
    onAddScreening(movie);
  };

  const renderAdminButtons = () => {
    if (getRole === UserRole.Admin) {
      return (
        <>
          <Button
            type="button"
            label="Add screening"
            size="large"
            variant="primary"
            onClickHandler={handleAddScreening}
          />
          <Button
            type="button"
            label="Delete"
            size="small"
            variant="secondary"
            onClickHandler={handleDelete}
          />
        </>
      );
    }
    return null;
  };

  return (
    <Card onClick={handleEdit}>
      <img src={movie.posterImage} alt="Movie Image" />
      <MovieInfo>
        <h3>{movie.name}</h3>
        <p>{movie.duration} min</p>
        <ul>
          {movie.genres.map((genre) => (
            <li key={genre.id}>{genre.name}</li>
          ))}
        </ul>
        {renderAdminButtons()}
      </MovieInfo>
    </Card>
  );
}
