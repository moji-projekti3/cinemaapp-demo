/* eslint-disable react/react-in-jsx-scope */
/* eslint-disable @typescript-eslint/no-empty-function */
import { JwtPayload, jwtDecode } from 'jwt-decode';
import {
  ReactNode,
  createContext,
  useContext,
  useState,
  useEffect,
} from 'react';

interface AuthProps {
  children?: ReactNode;
}

interface IAuthContext {
  authenticated: boolean;
  role: string;
  email: string;
  setAuthenticated: (newState: boolean) => void;
  decodeToken: (token: string) => void;
  logout: () => void;
}

const initialValue: IAuthContext = {
  authenticated: false,
  role: '',
  email: '',
  setAuthenticated: () => {},
  decodeToken: () => {},
  logout: () => {},
};

const AuthContext = createContext<IAuthContext>(initialValue);

export const AuthProvider = ({ children }: AuthProps) => {
  const [authenticated, setAuthenticated] = useState(
    initialValue.authenticated,
  );
  const [role, setRole] = useState(initialValue.role);
  const [email, setEmail] = useState(initialValue.email);

  interface CustomJwtPayload extends JwtPayload {
    'http://schemas.microsoft.com/ws/2008/06/identity/claims/role': string;
    'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name': string;
    email: string;
  }

  const decodeToken = (token: string) => {
    const decodedToken = jwtDecode<CustomJwtPayload>(token);
    const usersRole =
      decodedToken[
        'http://schemas.microsoft.com/ws/2008/06/identity/claims/role'
      ];
    const usersEmail =
      decodedToken[
        'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name'
      ];
    setAuthenticated(true);
    setEmail(usersEmail);
    setRole(usersRole);
  };

  useEffect(() => {
    const token = localStorage.getItem('token');
    if (token) {
      decodeToken(token);
    } else {
      setAuthenticated(false);
    }
  }, []);

  const logout = () => {
    localStorage.removeItem('token');
    setAuthenticated(false);
    setRole('');
    setEmail('');
  };

  return (
    <AuthContext.Provider
      value={{
        authenticated,
        setAuthenticated,
        logout,
        role,
        decodeToken,
        email,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => useContext(AuthContext);
