export default interface MovieInputDTO {
  name: string;
  originalName: string;
  duration: number;
  genreIds: number[];
}
