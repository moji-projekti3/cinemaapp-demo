export default interface ReserveTickets {
  movieScreeningId: number | undefined;
  userEmail: string;
  reservedSeats: number[];
}
