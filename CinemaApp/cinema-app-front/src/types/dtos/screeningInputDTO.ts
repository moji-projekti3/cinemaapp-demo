export interface AddScreeningDTO {
  movieId: number;
  date: string;
  time: string;
  ticketPrice: number;
  seatRows: number;
  seatColumns: number;
}
