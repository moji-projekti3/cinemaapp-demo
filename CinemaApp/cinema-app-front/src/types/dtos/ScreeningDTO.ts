import { Seat } from '../models/seat';

export interface ScreeningByIdDTO {
  id: number;
  date: string;
  ticketPrice: number;
  movieName: string;
  seats: Seat[];
}
