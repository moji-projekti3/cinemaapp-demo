// types/models/repertoireDisplay.ts
export interface ScreeningRepertoireDTO {
  id: number;
  date: string;
}

export interface MovieRepertoireDisplay {
  id: number;
  name: string;
  originalName: string;
  posterImage: string;
  duration: number;
  genres: string[];
  movieScreenings: ScreeningRepertoireDTO[];
}
export interface MovieScreeningDisplayDTOForUserReservationsDisplay {
  id: number;
  date: string;
  time: string;
  ticketPrice: number;
  movieName: string;
  moviePosterImage: string;
}