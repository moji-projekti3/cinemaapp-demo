import { Seat } from '../models/seat';
import { MovieScreeningDisplayDTOForUserReservationsDisplay } from './moviesDisplayForRepertoire';

export interface UserReservationsDisplay {
  id: number;
  userEmail: string;
  movieScreening: MovieScreeningDisplayDTOForUserReservationsDisplay;
  reservedSeats: Seat[];
  totalPrice: number;
}
