import Genre from './genre';
import { Screening } from './movieScreening';

export interface Movie {
  id: number;
  posterImage: string;
  name: string;
  originalName: string;
  duration: number;
  genres: Genre[];
  screenings: Screening[] | undefined;
}
