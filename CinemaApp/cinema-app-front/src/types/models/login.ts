export interface Login {
  email: string;
  password: string;
  formErrors?: { email: string; password: string };
  emailIsValid?: boolean;
  passwordIsValid?: boolean;
  formValid?: false;
}
