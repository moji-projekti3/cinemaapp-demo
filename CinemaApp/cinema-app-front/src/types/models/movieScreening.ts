import { Movie } from './movie';

export interface Screening {
  id: number;
  date: Date;
  ticketPrice: number;
  movieName: Movie;
}
