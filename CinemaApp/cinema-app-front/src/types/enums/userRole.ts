enum UserRole {
  Admin = 'Admin',
  Customer = 'Customer',
}

export default UserRole;
