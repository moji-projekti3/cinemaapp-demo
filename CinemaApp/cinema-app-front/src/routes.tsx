import { createBrowserRouter } from 'react-router-dom';
import AdminRoute from './components/admin/AdminRoute';
import GenresPage from './pages/GenresPage/GenresPage';
import Layout from './pages/Layout/Layout';
import LoginPage from './pages/LoginPage/LoginPage';
import MoviesPage from './pages/MoviesPage/MoviesPage';
import RegisterPage from './pages/RegisterPage/RegisterPage';
import Repertoire from './pages/RepertoirePage/Repertoire';
import Reservation from './pages/ReservationPage/Reservation';
import screeningService from './services/screeningService';
import ReservationsPage from './pages/UserReservationsPage/UserReservationsPage';

const router = createBrowserRouter([
  {
    path: '/',
    element: <Layout />,
    children: [
      { path: '', element: <Repertoire /> },
      { path: 'movies/repertoire/', element: <Repertoire /> },
      { path: 'login', element: <LoginPage /> },
      { path: 'register', element: <RegisterPage /> },
      {
        path: 'ticket-reservation/:id',
        loader: async ({ params }) => {
          return await screeningService.getScreening(+`${params.id}`);
        },
        element: <Reservation />,
      },
      {
        path: 'movies',
        element: <MoviesPage />,
      },
      {
        path: 'genres',
        element: (
          <AdminRoute>
            <GenresPage />
          </AdminRoute>
        ),
      },
      { path: 'ticketReservation', element: <ReservationsPage /> },
    ],
  },
]);

export default router;
