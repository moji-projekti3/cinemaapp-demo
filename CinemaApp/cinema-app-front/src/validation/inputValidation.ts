export const validateEmail = (email: string): boolean => {
  const regexForEmail = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  return regexForEmail.test(email.toLowerCase()) ? false : true;
};

export const validatePassword = (password: string): boolean => {
  const regexForPassword =
    /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,}$/;
  return regexForPassword.test(password) ? false : true;
};

export const validateTextInput = (
  value: string,
  minLength: number,
  maxLength: number,
): boolean =>
  value.trim().length < minLength || value.trim().length > maxLength
    ? true
    : false;

export const validateNumberInput = (
  value: number,
  minValue: number,
  maxValue: number,
): boolean => (value < minValue || value > maxValue ? true : false);

export const validateFileInput = (
  file: File | undefined,
  required: boolean,
  fileType: string,
): boolean =>
  required && !file
    ? false
    : file && !file.type.startsWith(fileType)
      ? false
      : true;
