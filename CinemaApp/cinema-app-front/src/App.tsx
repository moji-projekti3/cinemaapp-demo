import { RouterProvider } from 'react-router-dom';
import './App.css';
import router from './routes';
import { AuthProvider } from './Context/authContext/AuthContext';

export default function App() {
  return (
    <AuthProvider>
      <RouterProvider router={router} />
    </AuthProvider>
  );
}
