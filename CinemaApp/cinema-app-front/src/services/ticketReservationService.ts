import axios from 'axios';
import ReserveTickets from '../types/dtos/reservationDTO';
import { config } from 'process';

const reservationPath = '/ticketreservation';
const BASE_URL = process.env.REACT_APP_BASE_URL;
const client = axios.create({
  baseURL: BASE_URL,
});

const reserveTickets = async (data: ReserveTickets) => {
  return await client.post(`${reservationPath}`, data);
};

const getUsersReservations = async (
  userId: number,
  currentReservations: boolean,
) => {
  return await client.get(reservationPath, {
    params: { userId, currentReservations },
  });
};
const cancelReservation = async (reservationId: number) => {
  return await client.delete(`${reservationPath}/${reservationId}`);
};
export default { reserveTickets, cancelReservation, getUsersReservations };
