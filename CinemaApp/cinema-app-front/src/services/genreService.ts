import axios from 'axios';
import Genre from '../types/models/genre';
import PagedResponse from '../types/dtos/pagedResponse';
import authHeader from './auth/authorizationHeader';

const BASE_URL = process.env.REACT_APP_BASE_URL;
const genresPath = '/genres';
const client = axios.create({
  baseURL: BASE_URL,
});
const getGenres = (pageSize?: number, pageNumber?: number) => {
  let url = genresPath;
  if (pageSize !== undefined && pageNumber !== undefined) {
    url += `?pageSize=${pageSize}&pageNumber=${pageNumber}`;
  }
  return client.get<PagedResponse<Genre>>(url);
};

const addGenre = (name: string) => {
  return client.post<Genre>(genresPath, { name }, { headers: authHeader() });
};

const updateGenre = (id: number, name: string) => {
  return client.put<Genre>(`${genresPath}/${id}`, { name });
};
const deleteGenre = (id: number) => {
  return client.delete<Genre>(`${genresPath}/${id}`);
};
export default {
  getGenres,
  addGenre,
  updateGenre,
  deleteGenre,
};
