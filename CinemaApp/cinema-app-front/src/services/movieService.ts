import axios from 'axios';
import { Movie } from '../types/models/movie';
import PagedResponse from '../types/dtos/pagedResponse';
import { MovieRepertoireDisplay } from '../types/dtos/moviesDisplayForRepertoire';
import authHeader from './auth/authorizationHeader';
import { getCurrentUser } from './authService';

const moviesPath = '/movies';
const BASE_URL = process.env.REACT_APP_BASE_URL;
const client = axios.create({
  baseURL: BASE_URL,
});

client.interceptors.request.use(
  (config) => {
    const currentUser = getCurrentUser();
    if (currentUser) {
      config.headers.Authorization = `Bearer ${currentUser}`;
    }
    return config;
  },
  (error) => {
    Promise.reject(error);
  },
);

const getMovies = (pageSize?: number, pageNumber?: number) => {
  return client.get<PagedResponse<Movie>>(
    `${moviesPath}?pageSize=${pageSize}&pageNumber=${pageNumber}`,
    { headers: authHeader() },
  );
};

const getMovie = (id: number) => {
  return client.get<Movie>(`${id}`);
};

export const getRepertoire = async (date: Date, genre?: string) => {
  const formattedDate = new Intl.DateTimeFormat('en-CA').format(date);
  return await client.get<MovieRepertoireDisplay[]>(
    `${moviesPath}/repertoire`,
    {
      params: { date: formattedDate, genre },
    },
  );
};

const addMovie = (formData: FormData) => {
  return client.post<Movie>(moviesPath, formData, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  });
};

const updateMovie = (id: number, formData: FormData) => {
  return client.put<Movie>(`${moviesPath}/${id}`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  });
};

const deleteMovie = (id: number) => {
  return client.delete<Movie>(`${moviesPath}/${id}`);
};

export default {
  getMovies,
  getMovie,
  addMovie,
  updateMovie,
  deleteMovie,
  getRepertoire,
};
