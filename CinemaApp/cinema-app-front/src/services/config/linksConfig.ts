// services/config/linksConfig.ts
export interface LinkItem {
  linkText: string;
  href: string;
}

export interface LinksConfigType {
  guest: LinkItem[];
  admin: LinkItem[];
  customer: LinkItem[];
}

const LinksConfig: LinksConfigType = {
  guest: [
    { linkText: 'Home', href: '/' },
    { linkText: 'Login', href: '/login' },
    { linkText: 'Register', href: '/register' },
  ],
  admin: [
    { linkText: 'Home', href: '/' },
    { linkText: 'Movies', href: '/movies' },
    { linkText: 'Genres', href: '/genres' },
    { linkText: 'Reservations', href: '/ticketReservation' },
  ],
  customer: [
    { linkText: 'Home', href: '/' },
    { linkText: 'Movies', href: '/movies' },
    { linkText: 'Reservations', href: '/ticketReservation' },
  ],
};

export default LinksConfig;
