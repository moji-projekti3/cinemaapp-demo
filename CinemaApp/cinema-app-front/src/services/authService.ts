import axios from 'axios';
import { Login } from '../types/models/login';
import { Register } from '../types/models/register';

const BASE_URL = process.env.REACT_APP_BASE_URL;
const loginPath = '/authentication/login';
const registerPath = '/authentication/register';
const client = axios.create({
  baseURL: BASE_URL,
});

const loginUser = async (data: Login) => {
  try {
    const response = await client.post(loginPath, data);
    const token = response.data.token;
    if (token) {
      localStorage.setItem('token', token);
      client.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    }
    return response;
  } catch (error) {
    console.error('Login error:', error);
    throw error;
  }
};

const registerUser = async (data: Register) => {
  try {
    return await client.post(registerPath, data);
  } catch (error) {
    console.error('Login error:', error);
  }
};

export const getCurrentUser = () => {
  return localStorage.getItem('token');
};

export default { loginUser, registerUser };
