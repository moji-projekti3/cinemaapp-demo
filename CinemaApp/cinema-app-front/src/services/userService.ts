import axios from 'axios';

const userPath = '/user';
const BASE_URL = process.env.REACT_APP_BASE_URL;
const client = axios.create({
  baseURL: BASE_URL,
});

const getuserByEmail = (email: string) => {
  return client.get(`${userPath}/${email}`);
};
export default { getuserByEmail };
