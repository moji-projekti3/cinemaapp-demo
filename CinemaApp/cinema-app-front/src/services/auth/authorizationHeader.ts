export default function authHeader() {
  const userStr = localStorage.getItem('token');
  return { Authorization: 'Bearer ' + userStr };
}
