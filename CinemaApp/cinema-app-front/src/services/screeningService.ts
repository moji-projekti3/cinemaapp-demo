import axios from 'axios';
import { ScreeningByIdDTO } from '../types/dtos/ScreeningDTO';
import { AddScreeningDTO } from '../types/dtos/screeningInputDTO';
import authHeader from './auth/authorizationHeader';

const screeningPath = '/movieScreenings';
const BASE_URL = process.env.REACT_APP_BASE_URL;
const client = axios.create({
  baseURL: BASE_URL,
});

const getScreening = (id: number) => {
  return client.get<ScreeningByIdDTO>(`${screeningPath}/${id}`);
};

const addScreening = (screening: AddScreeningDTO) => {
  return client.post<AddScreeningDTO>(screeningPath, screening, {
    headers: authHeader(),
  });
};

export default { getScreening, addScreening };
