import styled from 'styled-components';

const ButtonContainer = styled.div`
  display: flex;
  justify-content: center;
`;
export { ButtonContainer };
export const ContainerWrapper = styled.div`
  position: relative;
  border: 2px solid black;
  width: 900px;
  background-color: rgba(255, 255, 128, 0.1);
  padding: 20px;
`;
