/* eslint-disable react/react-in-jsx-scope */
import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import { useAuth } from '../../Context/authContext/AuthContext';
import Button from '../../components/shared/button/Button';
import ReservationsList from '../../components/user/usersReservationList/UserReservationsList';
import ticketReservationService from '../../services/ticketReservationService';
import userService from '../../services/userService';
import { UserReservationsDisplay } from '../../types/dtos/userReservationsDisplayDTO';
import { ButtonContainer } from './UserReservationPage.styled';
import { useLoader } from '../../hooks/useLoader';

export default function ReservationsPage() {
  const { email: userEmail } = useAuth();
  const [userId, setUserId] = useState<number | null>(null);
  const [reservations, setReservations] = useState<UserReservationsDisplay[]>(
    [],
  );
  const [selectedButton, setSelectedButton] = useState<'current' | 'past'>(
    'current',
  );
  const [showCurrentReservations, setShowCurrentReservations] = useState(true);

  const { showLoader, hideLoader } = useLoader();

  useEffect(() => {
    if (userEmail) {
      fetchUser(userEmail);
    }
  }, [userEmail]);

  useEffect(() => {
    if (userId !== null) {
      fetchReservations(userId, showCurrentReservations);
    }
  }, [userId, showCurrentReservations]);

  const fetchUser = async (email: string) => {
    try {
      const response = await userService.getuserByEmail(email);
      setUserId(response.data.id);
    } catch (error) {
      toast.error('Failed to fetch user!');
    }
  };

  const fetchReservations = async (
    userId: number,
    currentReservations: boolean,
  ) => {
    if (userId === null) return;
    showLoader('Loading reservations...');
    try {
      const response = await ticketReservationService.getUsersReservations(
        userId,
        currentReservations,
      );
      setReservations(response.data);
      hideLoader();
    } catch (error) {
      toast.error('Error fetching reservations');
    }
  };

  const onCancelReservation = async (reservationId: number) => {
    try {
      await ticketReservationService.cancelReservation(reservationId);
      toast.success('Reservation canceled succesfully!');
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      fetchReservations(userId!, showCurrentReservations);
    } catch (error) {
      toast.error('Failed to cancel reservation.');
    }
  };

  return (
    <>
      <ButtonContainer>
        <Button
          label="Current reservations"
          size="medium"
          variant="info"
          isSelected={selectedButton === 'current'}
          onClickHandler={() => {
            setShowCurrentReservations(true);
            setSelectedButton('current');
          }}
        />
        <Button
          label="Past reservations"
          size="medium"
          variant="info"
          isSelected={selectedButton === 'past'}
          onClickHandler={() => {
            setShowCurrentReservations(false);
            setSelectedButton('past');
          }}
        />
      </ButtonContainer>
      {reservations.length > 0 ? (
        // <ContainerWrapper>
        <ReservationsList
          screeningReservations={reservations}
          onCancelReservation={onCancelReservation}
          isPastReservation={!showCurrentReservations}
        />
      ) : // </ContainerWrapper>
      null}
    </>
  );
}
