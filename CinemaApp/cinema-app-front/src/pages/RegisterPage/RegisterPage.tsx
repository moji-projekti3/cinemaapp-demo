import { toast } from 'react-toastify';
import RegisterForm from '../../components/shared/registration/RegisterForm';
import authService from '../../services/authService';
import { Register } from '../../types/models/register';
import { useNavigate } from 'react-router-dom';

export default function RegisterPage() {
  const navigate = useNavigate();

  const onSubmit = async (registerData: Register) => {
    try {
      await authService.registerUser(registerData);
      toast.success('Registration is successful!');
      navigate('/');
    } catch (error) {
      toast.error('Invalid register data, please try again!');
    }
  };
  return (
    <>
      <RegisterForm onSubmit={onSubmit} />
    </>
  );
}
