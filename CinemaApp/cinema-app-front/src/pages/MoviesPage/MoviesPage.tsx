/* eslint-disable react/react-in-jsx-scope */
import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import { useAuth } from '../../Context/authContext/AuthContext';
import MovieForm from '../../components/movie/movieForm/MovieForm';
import MoviesList from '../../components/movie/moviesList/MoviesList';
import ScreeningForm from '../../components/screening/screening/screeningForm/ScreeningForm';
import Button from '../../components/shared/button/Button';
import Modal from '../../components/shared/modal/Modal';
import Pagination from '../../components/shared/pagination/Pagination/Pagination';
import { useLoader } from '../../hooks/useLoader';
import movieService from '../../services/movieService';
import screeningService from '../../services/screeningService';
import { AddScreeningDTO } from '../../types/dtos/screeningInputDTO';
import { Movie } from '../../types/models/movie';
import { StyledButtonDiv } from '../GenresPage/GenresPage.styled';
import { getCurrentUser } from '../../services/authService';
import UserRole from '../../types/enums/userRole';

export default function MoviesPage() {
  const [movies, setMovies] = useState<Movie[]>([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const [editingMovie, setEditingMovie] = useState<Movie | null>(null);
  const [selectedMovie, setSelectedMovie] = useState<Movie | null>(null);
  const [isMovieModalOpen, setIsMovieModalOpen] = useState(false);
  const [isScreeningModalOpen, setIsScreeningModalOpen] = useState(false);
  const { showLoader, hideLoader } = useLoader();
  const { role: getRole } = useAuth();
  const pageSize = 6;

  useEffect(() => {
    getCurrentUser();
    fetchMovies();
  }, [currentPage]);

  const handleOpenModal = () => {
    setIsMovieModalOpen(true);
  };

  const handleCloseMovieModal = () => {
    setIsMovieModalOpen(false);
    setEditingMovie(null);
  };

  const handleOpenScreeningModal = (movie: Movie) => {
    setSelectedMovie(movie);
    setIsScreeningModalOpen(true);
  };

  const handleCloseScreeningModal = () => {
    setIsScreeningModalOpen(false);
    setSelectedMovie(null);
  };

  const fetchMovies = async () => {
    showLoader('Loading movies');
    try {
      const response = await movieService.getMovies(pageSize, currentPage);
      const pagedMoviesResponse = response.data;
      setMovies(pagedMoviesResponse.data);
      setTotalPages(pagedMoviesResponse.totalPages || 1);
      hideLoader();
    } catch (error) {
      toast.error(`Error occurred while fetching movies!`);
      hideLoader();
    }
  };

  const submitMovie = async (formData: FormData) => {
    try {
      if (editingMovie) {
        await movieService.updateMovie(editingMovie.id, formData);
        toast.success('Movie updated successfully!');
      } else {
        await movieService.addMovie(formData);
        handleCloseMovieModal();
        toast.success('Movie submitted successfully!');
      }
      fetchMovies();
    } catch (error) {
      toast.error('Error occurred while submitting movie');
    }
  };

  const submitScreening = async (formData: AddScreeningDTO) => {
    try {
      await screeningService.addScreening(formData);
      toast.success('Screening added successfully!');
      handleCloseScreeningModal();
      fetchMovies();
    } catch (error) {
      toast.error('Error occurred while adding screening');
    }
  };

  const editMovie = (movie: Movie) => {
    setEditingMovie(movie);
    handleOpenModal();
  };

  const handleDelete = async (movie: Movie) => {
    try {
      await movieService.deleteMovie(movie.id);
      fetchMovies();
      toast.success('Movie deleted successfully!');
    } catch (error) {
      toast.error('Error occurred while deleting movie!');
    }
  };

  const renderAdminButtons = () => {
    if (getRole === UserRole.Admin) {
      return (
        <>
          <Button
            variant="primary"
            label="Add movie"
            onClickHandler={handleOpenModal}
          />
        </>
      );
    }
    return null;
  };

  return (
    <>
      <StyledButtonDiv>{renderAdminButtons()}</StyledButtonDiv>
      <Modal isOpen={isMovieModalOpen} onClose={handleCloseMovieModal}>
        <MovieForm onSubmit={submitMovie} movie={editingMovie} />
      </Modal>
      <Modal isOpen={isScreeningModalOpen} onClose={handleCloseScreeningModal}>
        {selectedMovie && (
          <ScreeningForm
            onSubmit={submitScreening}
            screeningMovie={selectedMovie}
          />
        )}
      </Modal>
      <MoviesList
        movies={movies}
        onEdit={editMovie}
        onDelete={handleDelete}
        onAddScreening={handleOpenScreeningModal}
      />
      <Pagination
        currentPage={currentPage}
        lastPage={totalPages}
        maxLength={totalPages}
        setCurrentPage={setCurrentPage}
      />
    </>
  );
}
