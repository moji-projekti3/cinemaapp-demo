import { toast } from 'react-toastify';
import LoginForm from '../../components/shared/login/LoginForm';
import userService from '../../services/authService';
import { Login } from '../../types/models/login';
import { useNavigate } from 'react-router-dom';
import { useAuth } from '../../Context/authContext/AuthContext';

export default function LoginPage() {
  const { decodeToken } = useAuth();
  const navigate = useNavigate();

  const onSubmit = async (loginData: Login) => {
    try {
      const response = await userService.loginUser(loginData);
      toast.success('Successful login!');
      localStorage.setItem('token', response.data.token);
      decodeToken(response.data.token);
      navigate('/');
    } catch (error) {
      toast.error('Invalid credentials, please try again.');
      console.error('Login failed:', error);
    }
  };
  return (
    <>
      <LoginForm onSubmit={onSubmit} />
    </>
  );
}
