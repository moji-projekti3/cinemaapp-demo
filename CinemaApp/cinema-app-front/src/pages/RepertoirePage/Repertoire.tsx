/* eslint-disable react/react-in-jsx-scope */
// pages/RepertoirePage/Repertoire.tsx
import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import DatePickerList from '../../components/screening/datePicker/DatePickerList';
import ScreeningList from '../../components/screening/screening/screeningList/ScreeningList';
import genreService from '../../services/genreService';
import movieService from '../../services/movieService';
import { MovieRepertoireDisplay } from '../../types/dtos/moviesDisplayForRepertoire';
import Genre from '../../types/models/genre';
import { SelectGenre } from './Repertoire.styled';
import { useLoader } from '../../hooks/useLoader';

export default function Repertoire() {
  const navigate = useNavigate();
  const [selectedDate, setSelectedDate] = useState<Date | null>(new Date());
  const [movieScreenings, setMovieScreenings] = useState<
    MovieRepertoireDisplay[]
  >([]);
  const [genres, setGenres] = useState<Genre[]>([]);
  const [selectedGenre, setSelectedGenre] = useState<string>('');
  const { showLoader, hideLoader } = useLoader();

  useEffect(() => {
    if (selectedDate) {
      fetchScreenings(selectedDate);
      fetchGenres();
    }
  }, [selectedDate, selectedGenre]);

  const fetchGenres = async () => {
    try {
      const response = await genreService.getGenres();
      const pagedGenresResponse = response.data;
      setGenres(pagedGenresResponse.data);
    } catch (error) {
      toast.error('Error occurred while fetching genres!');
    }
  };

  const fetchScreenings = async (date: Date) => {
    showLoader('Loading repertoire...');
    try {
      if (selectedGenre) {
        const responseData = await movieService.getRepertoire(
          date,
          selectedGenre,
        );
        setMovieScreenings(responseData.data);
      } else {
        const response = await movieService.getRepertoire(date);
        setMovieScreenings(response.data);
      }
      hideLoader();
    } catch (error) {
      toast.error('Error fetching screenings');
    }
  };

  const handleDateClick = (date: Date) => {
    setSelectedDate(date);
  };
  const handleScreeningClick = async (screeningId: number) => {
    navigate(`/ticket-reservation/${screeningId}`, { replace: true });
  };
  return (
    <>
      <DatePickerList onDateClick={handleDateClick} />
      <SelectGenre
        value={selectedGenre}
        onChange={(e) => setSelectedGenre(e.target.value)}
      >
        <option value="">All genres</option>
        {genres.map((genre) => (
          <option key={genre.id} value={genre.name}>
            {genre.name}
          </option>
        ))}
      </SelectGenre>
      {movieScreenings.length > 0 ? (
        <ScreeningList
          movieScreenings={movieScreenings}
          onScreeningClick={handleScreeningClick}
        />
      ) : (
        <p style={{ color: 'black', alignSelf: 'center', fontSize: '3rem' }}>
          There is no screenings for today :(
        </p>
      )}
    </>
  );
}
