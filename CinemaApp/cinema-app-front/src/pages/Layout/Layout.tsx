/* eslint-disable react/react-in-jsx-scope */
import { Link, Outlet, useNavigate } from 'react-router-dom';
import { useAuth } from '../../Context/authContext/AuthContext';
import Button from '../../components/shared/button/Button';
import Footer from '../../components/shared/footer/Footer';
import Header from '../../components/shared/header/Header';
import { StyledMain } from './Layout.styled';
import LinksConfig, { LinkItem } from '../../services/config/linksConfig';
import UserRole from '../../types/enums/userRole';

export default function Layout() {
  const { authenticated, logout, role } = useAuth();
  const navigate = useNavigate();

  const handleLogout = () => {
    logout();
    navigate('/');
  };

  let links: LinkItem[] = [];
  if (role === UserRole.Admin) {
    links = LinksConfig.admin;
  } else if (role === UserRole.Customer) {
    links = LinksConfig.customer;
  } else {
    links = LinksConfig.guest;
  }

  return (
    <>
      <Header appTitle="Cinema App" LinkComponent={Link} links={links}>
        {authenticated && (
          <Button
            onClickHandler={handleLogout}
            size="medium"
            variant="secondary"
            label="Log out"
          />
        )}
      </Header>
      <StyledMain>
        <Outlet />
      </StyledMain>
      <Footer label="CinemaApp&copy;" devName="Radivoje Bjelic" />
    </>
  );
}
