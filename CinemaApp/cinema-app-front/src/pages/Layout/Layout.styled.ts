import styled from 'styled-components';

export const StyledMain = styled.main`
  display: flex;
  flex-direction: column;
  ul {
    align-self: center;
    list-style: none;
  }
`;
