import styled from 'styled-components';

export const ReservationScreenWrapper = styled.div`
  color: black;
  display: flex;
  height: 100%;
  margin-left: 0;
  border: 1px solid;
  background-color: rgba(255, 255, 255, 0.8);
  border-radius: 10px;
  box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);
  button {
    padding: 10px;
    margin: 10px;
  }
`;

export const SeatsSection = styled.section`
  position: absolute;
  right: 200px;
  top: 150px;
  width: 600px;
  height: 400px;

  h3 {
    border: 1px solid;
    margin: 0 auto;
    width: 75%;
    text-align: center;
  }
  div {
    margin: 40px;
    margin: 4 auto;
  }
`;

export const ScreeningDetails = styled.section`
  display: flexbox;
  margin: 0px;
  padding: 20px;
  h3 {
    margin: 0;
  }
  p {
    font-size: 0.9rem;
  }
`;

export const TotalPrice = styled.p`
  align-self: center;
  padding: 20px;
`;
