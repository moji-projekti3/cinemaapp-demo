import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { format } from 'date-fns';
import { toast } from 'react-toastify';

import SeatDisplay from '../../components/reservation/seatDisplay/SeatDisplay';
import { ScreeningByIdDTO } from '../../types/dtos/ScreeningDTO';
import {
  ReservationScreenWrapper,
  ScreeningDetails,
  SeatsSection,
  TotalPrice,
} from './Reservation.styled';
import screeningService from '../../services/screeningService';
import ticketReservationService from '../../services/ticketReservationService';
import ReserveTickets from '../../types/dtos/reservationDTO';
import Button from '../../components/shared/button/Button';
import Input from '../../components/shared/input/Input';
import { useAuth } from '../../Context/authContext/AuthContext';

export default function Reservation() {
  const { id } = useParams();
  const [screening, setScreening] = useState<ScreeningByIdDTO | null>(null);
  const { email: userEmail } = useAuth();
  const [email, setEmail] = useState(userEmail || '');
  const [selectedSeats, setSelectedSeats] = useState<number[]>([]);
  const [numberOfTickets, setNumberOfTickets] = useState(0);
  const [totalPrice, setTotalPrice] = useState(0);

  const navigate = useNavigate();

  const fetchScreening = async () => {
    if (id) {
      try {
        const response = await screeningService.getScreening(+id);
        setScreening(response.data);
      } catch (error) {
        toast.error('Error fetching screening details');
      }
    }
  };

  useEffect(() => {
    fetchScreening();
  }, [id]);

  const handleSeatSelect = (seatId: number) => {
    setSelectedSeats((prevSelectedSeats) =>
      prevSelectedSeats.includes(seatId)
        ? prevSelectedSeats.filter((id) => id !== seatId)
        : [...prevSelectedSeats, seatId],
    );
  };

  const handleEmailChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setEmail(event.target.value);
  };

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (!screening) {
      toast.error('Screening not found');
      return;
    }
    if (!email || selectedSeats.length === 0) {
      toast.error('Please fill in all fields');
      return;
    }

    const reservationData: ReserveTickets = {
      movieScreeningId: screening.id,
      userEmail: email,
      reservedSeats: selectedSeats,
    };

    try {
      await ticketReservationService.reserveTickets(reservationData);
      setScreening(screening);
      navigate('/movies/repertoire/', { replace: true });
      fetchScreening();
      toast.success('Reservation successful');
    } catch (error) {
      toast.error('Reservation failed');
    }
  };

  useEffect(() => {
    if (screening) {
      setTotalPrice(selectedSeats.length * screening.ticketPrice);
    }
  }, [selectedSeats, screening]);

  useEffect(() => {
    setNumberOfTickets(selectedSeats.length);
  }, [selectedSeats]);

  return (
    <>
      {screening && (
        <ReservationScreenWrapper>
          <section>
            <h2>Ticket reservation</h2>
            <ScreeningDetails>
              <h3>Screening details</h3>
              <p>Movie: {screening.movieName}</p>
              <p>Date: {format(new Date(screening.date), 'dd.MM.yyyy')}</p>
              <p>Time: {format(new Date(screening.date), 'HH:mm')}h</p>
              <p>Ticket Price: {screening.ticketPrice.toFixed(2)}$</p>
            </ScreeningDetails>
            <form onSubmit={handleSubmit}>
              <Input
                inputType="number"
                label="No. of tickets "
                value={`${numberOfTickets}`}
                disabled
              />
              <Input
                inputType="email"
                label="Email"
                placeholder="Enter your email"
                value={email}
                onChange={handleEmailChange}
              />
              <TotalPrice>Total price: {totalPrice.toFixed(2)}$</TotalPrice>
              <Button
                type="submit"
                label="Make reservation"
                size="small"
                variant="primary"
              />
            </form>
            <SeatsSection>
              <h3>Projection screen</h3>
              <SeatDisplay
                seats={screening.seats}
                selectedSeats={selectedSeats}
                onSeatSelect={handleSeatSelect}
              />
            </SeatsSection>
          </section>
        </ReservationScreenWrapper>
      )}
    </>
  );
}
