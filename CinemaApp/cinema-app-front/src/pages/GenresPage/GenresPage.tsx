/* eslint-disable react/react-in-jsx-scope */
import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import GenreForm, {
  GenreFormData,
} from '../../components/genre/genreForm/GenreForm';
import GenresList from '../../components/genre/genresList/GenresList';
import Button from '../../components/shared/button/Button';
import Modal from '../../components/shared/modal/Modal';
import Pagination from '../../components/shared/pagination/Pagination/Pagination';
import { useLoader } from '../../hooks/useLoader';
import genreService from '../../services/genreService';
import Genre from '../../types/models/genre';
import { StyledButtonDiv } from './GenresPage.styled';
import { useAuth } from '../../Context/authContext/AuthContext';
import UserRole from '../../types/enums/userRole';

export default function GenresPage() {
  const [genres, setGenres] = useState<Genre[]>([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const [editingGenre, setEditingGenre] = useState<Genre | null>(null);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const { showLoader, hideLoader } = useLoader();
  const pageSize = 5;
  const { role } = useAuth();

  const handleOpenModal = () => {
    setIsModalOpen(true);
  };

  const handleCloseModal = () => {
    setIsModalOpen(false);
    setEditingGenre(null);
  };

  useEffect(() => {
    fetchGenres();
  }, [currentPage]);

  const fetchGenres = async () => {
    showLoader('Loading genres');
    try {
      const response = await genreService.getGenres(pageSize, currentPage);
      const pagedGenresResponse = response.data;
      setGenres(pagedGenresResponse.data);
      setTotalPages(pagedGenresResponse.totalPages || 1);
      hideLoader();
    } catch (error) {
      toast.error('Error occurred while fetching genres!');
      hideLoader();
    }
  };

  const submitGenre = async (data: GenreFormData) => {
    try {
      if (role === UserRole.Admin) {
        if (editingGenre) {
          await genreService.updateGenre(editingGenre.id, data.name);
          toast.success('Genre updated successfully!');
        } else {
          await genreService.addGenre(data.name);
          toast.success('Genre added successfully!');
        }
        handleCloseModal();
        fetchGenres();
      } else {
        toast.error('Unauthorized access!');
      }
    } catch (error) {
      toast.error('Error occurred while submitting genre!');
    }
  };

  const editGenre = (genre: Genre) => {
    if (role === UserRole.Admin) {
      setEditingGenre(genre);
      handleOpenModal();
    }
  };

  const deleteGenre = async (genre: Genre) => {
    try {
      if (role === UserRole.Admin) {
        await genreService.deleteGenre(genre.id);
        fetchGenres();
        toast.success('Genre deleted successfully!');
      }
    } catch (error) {
      toast.error('Error occurred while deleting genre!');
    }
  };

  return (
    <>
      <StyledButtonDiv>
        <Button
          variant="secondary"
          label="Add Genre"
          onClickHandler={handleOpenModal}
        />
      </StyledButtonDiv>
      <Modal isOpen={isModalOpen} onClose={handleCloseModal}>
        <GenreForm onSubmit={submitGenre} genre={editingGenre} />
      </Modal>
      <GenresList genres={genres} onEdit={editGenre} onDelete={deleteGenre} />
      <Pagination
        currentPage={currentPage}
        lastPage={totalPages}
        maxLength={totalPages}
        setCurrentPage={setCurrentPage}
      />
    </>
  );
}
