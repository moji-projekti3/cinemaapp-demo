import styled from 'styled-components';

export const StyledMain = styled.main`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  header {
    nav {
      display: flex;
      align-items: center;
    }
    ul {
      display: inline-flex;
      list-style-type: none;
      a {
        margin-left: 20px;
        text-decoration: none;
        color: black;
        &:hover {
          text-decoration: underline;
        }
      }
    }
  }
`;
export const StyledButtonDiv = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;
