﻿using DAL.Data;
using DOMAIN.Entities;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;
using System.IO;

namespace CinemaApp.Api.IntegrationTests
{
    internal class CinemaAppWebApplicationFactory : WebApplicationFactory<Program>
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureAppConfiguration((context, config) =>
            {
                config.AddJsonFile("appsettings.Test.json", optional: true);
            });

            builder.ConfigureTestServices(services =>
            {
                services.RemoveAll(typeof(DbContextOptions<CinemaContext>));

                services.AddDbContext<CinemaContext>(options =>
                {
                    options.UseInMemoryDatabase("TestDatabase_" + Guid.NewGuid()); 
                });

                services.AddAuthentication("Test")
                    .AddScheme<AuthenticationSchemeOptions, TestAuthHandler>("Test", options => { });

                var serviceProvider = services.BuildServiceProvider();
                using (var scope = serviceProvider.CreateScope())
                {
                    var dbContext = scope.ServiceProvider.GetRequiredService<CinemaContext>();

                    dbContext.Database.EnsureCreated();
                    try
                    {
                        SeedDatabase(dbContext);
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            });
        }

        private static void SeedDatabase(CinemaContext context)
        {
            context.Genres.Add(new Genre { Id = 1, Name = "Horror" });
            context.Genres.Add(new Genre { Id = 2, Name = "Crime" });
            context.SaveChanges();
        }
    }
}
