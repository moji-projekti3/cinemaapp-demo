using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using API.Controllers;
using DAL.Data;
using DOMAIN.DTOs;
using DOMAIN.GenreDTOs.DTOs;
using DOMAIN.MovieDTOs.DTOs;
using DOMAIN.MoviscreeningDTOs.DTOs;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xunit;

namespace CinemaApp.Api.IntegrationTests
{
    public class CinemaAppApiTests 
    {
       
        [Fact]
        public async Task AddsGenre()
        {
            // Arrange
            var client = new CinemaAppWebApplicationFactory();
            var genreInputDTO = new GenreInputDTO
            {
                Name = "Comedy"
            };

            var application = client.CreateClient();
            // Act
            var response = await application.PostAsJsonAsync("/api/genres", genreInputDTO);

            // Assert
            response.EnsureSuccessStatusCode();
            var genreResponse = await response.Content.ReadFromJsonAsync<GenreDisplayDTO>();
            genreResponse.Should().NotBeNull();
            genreResponse?.Name.Should().Be("Comedy");
        }

        [Fact]
        public async Task AddsMovieScreening()
        {
            // Arrange
            var factory = new WebApplicationFactory<Program>();
            var client = factory.CreateClient();
            var token = GenerateJwtToken();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var movieScreeningInputDTO = new MovieScreeningInputDTO
            {
                Date = DateTime.UtcNow.AddDays(6), 
                TicketPrice = 10.5m,
                SeatRows = 10,
                SeatColumns = 8,
                MovieId = 1
            };

            // Act
            var response = await client.PostAsJsonAsync("/api/moviescreenings", movieScreeningInputDTO);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Created);

            var movieScreeningResponse = await response.Content.ReadFromJsonAsync<MovieScreeningDisplayDTO>();
            movieScreeningResponse.Should().NotBeNull();
            movieScreeningResponse?.Date.Should().Be(movieScreeningInputDTO.Date);
            movieScreeningResponse.TicketPrice.Should().Be(movieScreeningInputDTO.TicketPrice);

            
        }

        [Fact]
        public async Task AddsMovie()
        {
            // Arrange
            var factory = new WebApplicationFactory<Program>();
            
            var movieInputDTO = new MovieInputDTO
            {
                OriginalName = "Some Comedy",
                Name = "Comedy",
                Duration = 120,
                GenreIds = new List<long> { 1, 2 }
            };

            // Serialize movieInputDTO to JSON
            var movieJson = JsonConvert.SerializeObject(movieInputDTO);

            // Create a multipart form content
            var content = new MultipartFormDataContent();

            // Add the movie JSON as a string content part named 'movie'
            content.Add(new StringContent(movieJson, Encoding.UTF8, "multipart/form-data"), "movie");

            // Load poster image from a file and add it to the content
            // NOTE: Adjust the posterImagePath or leave it empty if not including a poster image
            var posterImagePath = ""; // Provide the correct path to your image if needed
            if (!string.IsNullOrEmpty(posterImagePath))
            {
                using (var fileStream = new FileStream(posterImagePath, FileMode.Open, FileAccess.Read))
                {
                    var fileContent = new StreamContent(fileStream);
                    fileContent.Headers.ContentType = new MediaTypeHeaderValue("image/png");
                    content.Add(fileContent, "posterImage", "No-Image-Placeholder.png");
                }
            }

            var client = factory.CreateClient();

            // Act
            var response = await client.PostAsync("/api/movies", content);

            // Assert
            response.EnsureSuccessStatusCode();
            var movieResponse = await response.Content.ReadFromJsonAsync<MoviesDisplayDTO>();
            movieResponse.Should().NotBeNull();
            movieResponse.Name.Should().Be("Comedy");
        }

        private string GenerateJwtToken()
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("this is my custom Secret key for authentication"));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                issuer: "https://localhost:50001",
                audience: "https://localhost:50001",
                expires: DateTime.Now.AddHours(1),
                signingCredentials: credentials
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }


    }
}
