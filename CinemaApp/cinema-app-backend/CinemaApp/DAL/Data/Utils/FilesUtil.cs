using DOMAIN.Exceptions;
using DOMAIN.Validation;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;

namespace DAL.Data.Utils
{
    public static class FilesUtil
    {
        public static string Save(IFormFile file, string saveToPath)
        {
            if (file?.Length == 0)
            {
                return null;
            }
            var fileName = $"{Guid.NewGuid()}_{Path.GetFileName(file.FileName)}";

            var fullPath = Path.Combine(saveToPath, fileName);

            // fullPath = fullPath.Replace("\\", "/");

            using (var stream = new FileStream(fullPath, FileMode.Create))
            {
                file.CopyTo(stream);
            }

            return fullPath;
        }


        public static string GetPath(string filePath, string baseUrl)
        {
            if (string.IsNullOrEmpty(filePath) || string.IsNullOrEmpty(baseUrl))
            {
                return null;
            }

            var fileName = Path.GetFileName(filePath);
            return $"{baseUrl.TrimEnd('/')}/{fileName.TrimStart('/')}";
        }

        public static Tuple<FileStream, string> Read(string filePath)
        {
            if (!File.Exists(filePath))
            {
                return null;
            }
            var file = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            var extension = Path.GetExtension(filePath);

            return new Tuple<FileStream, string>(file, extension);
        }

        public static void Delete(string filePath)
        {
            if (string.IsNullOrEmpty(filePath))
            {
                throw new ArgumentException("File path is null or empty", nameof(filePath));
            }

            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
            else
            {
                throw new FileNotFoundException("File not found", filePath);
            }
        }
    }
}
