using System;
using System.Collections.Generic;
using System.Reflection;
using DOMAIN.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DAL.Data
{
    public class CinemaContext : IdentityDbContext<ApplicationUser>
    {
        public CinemaContext(DbContextOptions options)
            : base(options) { }

        public DbSet<Genre> Genres { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<MovieScreening> MovieScreenings { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Seat> Seats { get; set; }
        public DbSet<TicketReservation> Reservations { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            modelBuilder
                .Entity<Movie>()
                .HasMany(m => m.Genres)
                .WithMany(g => g.Movies)
                .UsingEntity(join => join.ToTable("MovieGenre"));

            modelBuilder.Entity<Movie>().ToTable("Movies").HasMany(m => m.MovieScreenings);

            modelBuilder
                .Entity<MovieScreening>()
                .ToTable("MovieScreenings")
                .HasOne(ms => ms.Movie)
                .WithMany(m => m.MovieScreenings);

            modelBuilder.Entity<MovieScreening>().HasMany(ms => ms.Seats).WithOne(s => s.Screening);

            modelBuilder
                .Entity<MovieScreening>()
                .Property(ms => ms.TicketPrice)
                .HasColumnType("decimal(18,2)");

            modelBuilder
                .Entity<TicketReservation>()
                .ToTable("TicketReservation")
                .HasOne(r => r.MovieScreening);

            modelBuilder.Entity<User>().ToTable("Users").HasMany(r => r.Reservations);

            modelBuilder
                .Entity<User>()
                .Property(u => u.UsersRole)
                .IsRequired()
                .HasConversion<string>();

            modelBuilder.Entity<User>().HasIndex(u => u.Email).IsUnique();

            SeedData(modelBuilder);
        }

        private void SeedData(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Genre>().HasData(
                new Genre { Id = 1, Name = "Horror" ,IsDeleted=false},
                    new Genre { Id = 2, Name = "Crime",IsDeleted=false },
                    new Genre { Id = 3, Name = "Comedy" ,IsDeleted=false},
                    new Genre { Id = 4, Name = "Drama" ,IsDeleted=false},
                    new Genre { Id = 5, Name = "Action" ,IsDeleted=false},
                    new Genre { Id = 6, Name = "Action comedy" ,IsDeleted=false},
                    new Genre { Id = 7, Name = "War" ,IsDeleted=false},
                    new Genre { Id = 8, Name = "Thriller" ,IsDeleted=false},
                    new Genre { Id = 9, Name = "Love" ,IsDeleted=false},
                     new Genre { Id = 10, Name = "Romantic comedy",IsDeleted=false }
            );

            var hasher = new PasswordHasher<ApplicationUser>();
            var admin = new ApplicationUser
            {
                Id = "1",
                UserName = "admin@cinemaapp.com",
                NormalizedUserName = "ADMIN@CINEMAAPP.COM",
                Email = "admin@cinemaapp.com",
                NormalizedEmail = "ADMIN@CINEMAAPP.COM",
                EmailConfirmed = true,
                Name = "Admin",
                DateOfBirth = new DateTime(1990, 1, 1),
                UsersRole = UsersRole.Admin
            };
            admin.PasswordHash = hasher.HashPassword(admin, "Admin123!");
            
            modelBuilder.Entity<ApplicationUser>().HasData(admin);
             modelBuilder.Entity<User>().HasData(
                new User{Id = 1, Name = "Admin", DateOfBirth=new DateTime(1990, 1, 1), Email="admin@cinemaapp.com", IsAuthenticated=true, UsersRole=0}
             );
            modelBuilder.Entity<IdentityRole>().HasData(
                new IdentityRole
                {
                    Id = "1",
                    Name = "Admin",
                    NormalizedName = "ADMIN"
                }
            );

            modelBuilder.Entity<IdentityUserRole<string>>().HasData(
                new IdentityUserRole<string>
                {
                    RoleId = "1",
                    UserId = "1"
                }
            );
        }
    }
}

