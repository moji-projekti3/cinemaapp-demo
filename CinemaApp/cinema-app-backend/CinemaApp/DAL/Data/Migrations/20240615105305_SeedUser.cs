﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DAL.Data.Migrations
{
    /// <inheritdoc />
    public partial class SeedUser : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "1aa1d468-e804-49f8-a117-d69fff6f0b44", "AQAAAAIAAYagAAAAEACE5yT8amOVtVnJwvEBrbvBaviLcS+vcSRY8HiBkVovMRJgPoOmSab98UqhOajA1A==", "84a020ec-1196-45aa-b27b-7b456dbd6025" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "DateOfBirth", "Email", "IsAuthenticated", "Name", "UsersRole" },
                values: new object[] { 1L, new DateTime(1990, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "admin@cinemaapp.com", true, "Admin", "Admin" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1L);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "795f1f76-7104-4b5d-ae8e-96e2ebfe9de2", "AQAAAAIAAYagAAAAEOVHhBfXBQhkh5xTKaXmDbi8TziI8Kx+LsXKPh+zVENqzPJBueD0y2xw8FsJgt3JZA==", "13535eac-77d9-4ec4-acfd-ba2c72ec5d68" });
        }
    }
}
