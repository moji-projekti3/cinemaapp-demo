using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Interfaces;
using DOMAIN.Entities;
using Microsoft.EntityFrameworkCore;

namespace DAL.Data.Repository
{
    public class UsersRepository : IUserRepository
    {
        private readonly CinemaContext _context;

        public UsersRepository(CinemaContext context)
        {
            _context = context;
        }

        public async Task<User> GetUserByEmail(string email)
        {
            return await _context.Users.FirstOrDefaultAsync(user =>
                user.Email.ToLower() == email.ToLower()
            );
        }

        public async Task<User> GetUserById(long id)
        {
            return await _context.Users.FirstOrDefaultAsync(user => user.Id == id);
        }
    }
}
