using BLS.Interfaces;
using DAL.Interfaces;

namespace DAL.Data.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly CinemaContext _context;
        private IGenreRepository _genres;
        private IMoviesRepository _movies;
        private IMovieScreeningRepository _screenings;
        private ITicketReservationRepository _ticketReservations;
        private IUserRepository _userRepository;

        public UnitOfWork(CinemaContext context)
        {
            _context = context;
        }

        public IGenreRepository Genres
        {
            get
            {
                if (_genres == null)
                {
                    _genres = new GenresRepository(_context);
                }
                return _genres;
            }
        }
        public IMoviesRepository Movies => _movies ??= new MoviesRepository(_context);

        public IMovieScreeningRepository Screenings =>
            _screenings ??= new MovieScreeningsRepository(_context);

        public ITicketReservationRepository TicketReservations =>
            _ticketReservations ??= new TicketReservationRepository(_context);
        public IUserRepository UserRepository => _userRepository ??= new UsersRepository(_context);

        public async Task<int> Complete()
        {
            return await _context.SaveChangesAsync();
        }
    }
}
