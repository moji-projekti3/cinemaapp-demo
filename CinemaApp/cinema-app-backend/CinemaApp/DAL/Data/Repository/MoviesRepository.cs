using BLS.Interfaces;
using DOMAIN.Entities;
using DOMAIN.ValueEntities;
using Microsoft.EntityFrameworkCore;
using static DAL.Constants.Constants;

namespace DAL.Data.Repository
{
    public class MoviesRepository : IMoviesRepository
    {
        private readonly CinemaContext _context;

        public MoviesRepository(CinemaContext context)
        {
            _context = context;
        }

        public async Task<Movie> AddMovieAsync(Movie movie)
        {
            await _context.AddAsync(movie);
            return movie;
        }

        public async Task DeleteMovieAsync(long id)
        {
            var movie = await _context.Movies.FindAsync(id);
            movie.IsDeleted = true;
        }

        public async Task<Movie> GetMovieByIdAsync(long id)
        {
            var movie = await _context
                .Movies.Include(movie => movie.Genres)
                .Include(movie => movie.MovieScreenings)
                .Where(movie => !movie.IsDeleted)
                .FirstOrDefaultAsync(movie => movie.Id == id);
            return movie;
        }

        public async Task<List<Movie>> GetMoviesWithScreeningsForDayAsync(
            DateTime date,
            string sort = null,
            string genre = null
        )
        {
            var query = _context
                .Movies.Include(movie => movie.MovieScreenings)
                .ThenInclude(screening => screening.Seats)
                .Where(movie => !movie.IsDeleted)
                .Where(movie =>
                    movie.MovieScreenings.Any(screening =>
                        screening.Date.Date == date.Date && !screening.IsDeleted
                    )
                )
                .Include(movie => movie.Genres)
                .AsQueryable();

            if (!string.IsNullOrEmpty(genre))
            {
                query = query.Where(movie =>
                    movie.Genres.Any(genres => genres.Name.ToLower() == genre.ToLower())
                );
            }

            if (sort == Sort.Chronologically)
            {
                query = query.OrderBy(movie =>
                    movie
                        .MovieScreenings.Where(screening => screening.Date.Date == date.Date)
                        .OrderBy(screening => screening.Date.Date.TimeOfDay)
                        .FirstOrDefault()
                );
            }
            else if (sort == Sort.Alphabetically)
            {
                query = query.OrderBy(movie => movie.Name);
            }

            return await query.Distinct().AsNoTracking().ToListAsync();
        }

        public async Task<PagedResponseOffset<Movie>> GetMoviesAsync(
            List<string> startLetter = null,
            List<string> containsLetters = null,
            PagedResponseOffset<Movie> pagedResponse = null
        )
        {
            IQueryable<Movie> query = _context
                .Movies.Include(movie => movie.Genres)
                .Include(movie => movie.MovieScreenings)
                .Where(movie => !movie.IsDeleted).AsNoTracking()
                .AsQueryable();
            if (startLetter != null && startLetter.Count > 0)
            {
                foreach (var letter in startLetter)
                {
                    query = query.Where(movie =>
                        letter.Contains(movie.Name.ToLower().Substring(0, 1))
                    );
                }
            }
            if (containsLetters != null && containsLetters.Any())
            {
                foreach (var letters in containsLetters)
                {
                    query = query.Where(movie => movie.Name.ToLower().Contains(letters.ToLower()));
                }
            }
            pagedResponse.TotalRecords = query.Count();
            if (pagedResponse?.PageNumber > 0 && pagedResponse?.PageSize > 0)
            {
                pagedResponse.TotalPages = (int)
                    Math.Ceiling(
                        (decimal)pagedResponse.TotalRecords / (decimal)pagedResponse.PageSize
                    );
                pagedResponse.Data = await query
                    .Skip((pagedResponse.PageNumber - 1) * pagedResponse.PageSize)
                    .Take(pagedResponse.PageSize)
                    .ToListAsync();
            }
            else
            {
                pagedResponse.TotalPages = 1;
                pagedResponse.PageNumber = 1;
                pagedResponse.PageSize = pagedResponse.TotalRecords;
                pagedResponse.Data = await query.ToListAsync();
            }
            return pagedResponse;
        }

        public async Task<List<Movie>> GetMoviesWithIds(List<long> ids)
        {
            return await _context
                .Movies.Where(movie => ids.Contains(movie.Id) && !movie.IsDeleted)
                .ToListAsync();
        }

        public async Task<bool> MovieExistsAsync(string movieName)
        {
            return await _context.Movies.AnyAsync(movie =>
                movie.Name.ToLower() == movieName.ToLower() && !movie.IsDeleted
            );
        }
    }
}
