using DAL.Interfaces;
using DOMAIN.DTOs;
using DOMAIN.Entities;
using DOMAIN.ValueEntities;
using Microsoft.EntityFrameworkCore;

namespace DAL.Data.Repository
{
    public class MovieScreeningsRepository : IMovieScreeningRepository
    {
        private readonly CinemaContext _context;

        public MovieScreeningsRepository(CinemaContext context)
        {
            _context = context;
        }

        public async Task<MovieScreening> AddCinemaRepertoireAsync(MovieScreening movieScreening)
        {
            await _context.AddAsync(movieScreening);
            return movieScreening;
        }

        public async Task DeleteMovieScreeningAsync(long id)
        {
            var movieScreening = await _context.MovieScreenings.FindAsync(id);
            movieScreening.IsDeleted = true;
        }

        public async Task<MovieScreening> GetMovieScreeningByIdAsync(long id)
        {
            var movieScreening = await _context
                .MovieScreenings.Include(screening => screening.Seats)
                .Include(screening => screening.Movie)
                .Include(screening => screening.Movie.Genres)
                .Where(screening => !screening.IsDeleted)
                .FirstOrDefaultAsync(screening => screening.Id == id);
            return movieScreening;
        }

        public async Task<List<MovieScreening>> GetScreeningsByMovieIdAsync(long movieId)
        {
            return await _context
                .MovieScreenings.Include(screening => screening.Seats).Where(screening =>
                    screening.Movie.Id == movieId && !screening.IsDeleted
                )
                .ToListAsync();
        }

        public async Task<PagedResponseOffset<MovieScreening>> GetMovieScreeningsAsync(
            List<string> startLetter = null,
            string containsLetters = null,
            PagedResponseOffset<MovieScreening> pagedResponse = null
        )
        {
            IQueryable<MovieScreening> query = _context
                .MovieScreenings.Include(screening => screening.Seats)
                .Include(screening => screening.Movie)
                .Include(screening => screening.Movie.Genres)
                .Where(screening => !screening.IsDeleted)
                .AsQueryable();
            if (startLetter != null && startLetter.Count > 0)
            {
                foreach (var letter in startLetter)
                {
                    query = query.Where(movie =>
                        letter.Contains(movie.Movie.Name.ToLower().Substring(0, 1))
                    );
                }
            }
            if (containsLetters != null && containsLetters.Any())
            {
                query = query.Where(movie =>
                    movie.Movie.Name.ToLower().Contains(containsLetters.ToLower())
                );
            }
            pagedResponse.TotalRecords = query.Count();
            if (pagedResponse?.PageNumber > 0 && pagedResponse?.PageSize > 0)
            {
                pagedResponse.TotalPages = (int)
                    Math.Ceiling(
                        (decimal)pagedResponse.TotalRecords / (decimal)pagedResponse.PageSize
                    );
                pagedResponse.Data = await query
                    .Skip((pagedResponse.PageNumber - 1) * pagedResponse.PageSize)
                    .Take(pagedResponse.PageSize)
                    .ToListAsync();
            }
            else
            {
                pagedResponse.TotalPages = 1;
                pagedResponse.PageNumber = 1;
                pagedResponse.PageSize = pagedResponse.TotalRecords;
                pagedResponse.Data = await query.ToListAsync();
            }

            return pagedResponse;
        }
    }
}
