using System.Reflection.Metadata.Ecma335;
using DAL.Interfaces;
using DOMAIN.Entities;
using Microsoft.EntityFrameworkCore;

namespace DAL.Data.Repository
{
    public class TicketReservationRepository : ITicketReservationRepository
    {
        private readonly CinemaContext context;

        public TicketReservationRepository(CinemaContext context)
        {
            this.context = context;
        }

        public async Task CancelReservation(long id)
        {
            var reservation = await context.Reservations.FindAsync(id);
            reservation.ReservedSeats.ForEach(seat => seat.IsReserved = false);
            reservation.IsCancelled = true;
        }

        public async Task CancelReservationsForMovieScreening(long screeningId)
        {
            var reservations = context.Reservations.Where(reservation =>
                !reservation.IsCancelled && reservation.MovieScreening.Id == screeningId
            );
            if (!reservations.Any())
            {
                return;
            }
            await reservations.ForEachAsync(r => r.IsCancelled = true);
        }

        public async Task<List<TicketReservation>> GetReservationByScreeningIdAsync(long id)
        {
            var query = context.Reservations.AsQueryable();
            var reservation = query
                .Include(screening => screening.MovieScreening)
                .Where(screening => screening.MovieScreening.Id == id && !screening.IsCancelled)
                .Where(reservation => !reservation.IsCancelled);
            ;
            return await reservation.ToListAsync();
        }

        public async Task<List<TicketReservation>> GetUsersReservations(
            long userId,
            bool getCurrentReservations
        )
        {
            var query = context.Reservations.AsQueryable();
            if (getCurrentReservations)
            {
                return await query
                    .Include(r => r.MovieScreening)
                    .ThenInclude(ms => ms.Seats)
                    .Include(m => m.MovieScreening.Movie)
                    .Where(r =>
                        r.User.Id == userId
                        && r.MovieScreening.Date >= DateTime.Today
                        && !r.IsCancelled
                    )
                    .ToListAsync();
            }
            else
            {
                return await query
                    .Include(r => r.MovieScreening)
                    .ThenInclude(ms => ms.Seats.Where(r => r.IsReserved))
                    .Include(m => m.MovieScreening.Movie)
                    .Where(r => r.User.Id == userId && r.MovieScreening.Date < DateTime.Today)
                    .ToListAsync();
            }
        }

        public async Task<TicketReservation> GetReservationByIdAsync(long id)
        {
            var reservation = await context
                .Reservations.Include(screening => screening.MovieScreening)
                .ThenInclude(screening => screening.Seats)
                .Where(reservation => !reservation.IsCancelled)
                .FirstOrDefaultAsync(reservation => reservation.Id == id);
            return reservation;
        }

        public async Task<List<Seat>> GetSeatsWithIds(List<long> ids)
        {
            return await context.Seats.Where(seat => ids.Contains(seat.Id)).ToListAsync();
        }

        public async Task<TicketReservation> ReserveTicketsAsync(
            TicketReservation ticketReservation
        )
        {
            await context.AddAsync(ticketReservation);
            return ticketReservation;
        }
    }
}
