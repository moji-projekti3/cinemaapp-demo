using BLS.Interfaces;
using DOMAIN.Entities;
using DOMAIN.ValueEntities;
using Microsoft.EntityFrameworkCore;

namespace DAL.Data.Repository
{
    public class GenresRepository : IGenreRepository
    {
        private readonly CinemaContext _context;

        public GenresRepository(CinemaContext context)
        {
            _context = context;
        }

        public async Task<Genre> AddGenreAsync(Genre genre)
        {
            var newGenre = await _context.AddAsync(genre);
            return newGenre.Entity;
        }

        public async Task<Genre> GetGenreByNameAsync(string genreName)
        {
            return await _context
                .Genres.Where(genre => !genre.IsDeleted)
                .FirstOrDefaultAsync(genre => genre.Name.ToLower() == genreName.ToLower());
        }

        public async Task DeleteGenreAsync(long id)
        {
            var genre = await _context.Genres.FindAsync(id);
            if (genre == null)
            {
                return;
            }
            genre.IsDeleted = true;
        }

        public async Task<Genre> GetGenreByIdAsync(long id)
        {
            var genre = await _context
                .Genres.Where(genre => !genre.IsDeleted)
                .FirstOrDefaultAsync(genre => genre.Id == id);
            return genre;
        }

        public async Task<PagedResponseOffset<Genre>> GetGenresAsync(
            List<string> startLetter = null,
            string containsLetters = null,
            PagedResponseOffset<Genre> pagedResponse = null
        )
        {
            IQueryable<Genre> query = _context
                .Genres.Where(genre => !genre.IsDeleted)
                .AsQueryable();

            if (startLetter != null && startLetter.Count > 0)
            {
                query = query.Where(genre =>
                    startLetter.Contains(genre.Name.ToLower().Substring(0, 1))
                );
            }
            if (containsLetters != null && containsLetters.Any())
            {
                query = query.Where(genre =>
                    genre.Name.ToLower().Contains(containsLetters.ToLower())
                );
            }

            pagedResponse.TotalRecords = query.Count();
            if (pagedResponse?.PageNumber > 0 && pagedResponse?.PageSize > 0)
            {
                pagedResponse.TotalPages = (int)
                    Math.Ceiling(
                        (decimal)pagedResponse.TotalRecords / (decimal)pagedResponse.PageSize
                    );
                pagedResponse.Data = await query
                    .Skip((pagedResponse.PageNumber - 1) * pagedResponse.PageSize)
                    .Take(pagedResponse.PageSize)
                    .ToListAsync();
            }
            else
            {
                pagedResponse.TotalPages = 1;
                pagedResponse.PageNumber = 1;
                pagedResponse.PageSize = pagedResponse.TotalRecords;
                pagedResponse.Data = await query.ToListAsync();
            }

            return pagedResponse;
        }

        public async Task<List<Genre>> GetGenresWithIds(List<long> ids)
        {
            return await _context
                .Genres.Where(genre => ids.Contains(genre.Id))
                .Where(genre => genre.IsDeleted == false)
                .ToListAsync();
        }
    }
}
