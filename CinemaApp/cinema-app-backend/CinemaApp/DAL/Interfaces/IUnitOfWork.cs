using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLS.Interfaces;
using DAL.Data.Repository;

namespace DAL.Interfaces
{
    public interface IUnitOfWork 
    {
        IGenreRepository Genres { get; }
        IMoviesRepository Movies { get; }
        IMovieScreeningRepository Screenings { get; }
        ITicketReservationRepository TicketReservations { get; }
        IUserRepository UserRepository { get; }
        Task<int> Complete();
    }
}
