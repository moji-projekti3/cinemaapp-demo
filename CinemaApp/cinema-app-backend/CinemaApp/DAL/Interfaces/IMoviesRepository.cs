using DOMAIN.Entities;
using DOMAIN.ValueEntities;
using Microsoft.AspNetCore.Http;

namespace BLS.Interfaces
{
    public interface IMoviesRepository
    {
        Task<Movie> GetMovieByIdAsync(long id);
        Task<PagedResponseOffset<Movie>> GetMoviesAsync(
            List<string> startLetter = null,
            List<string> containsLetters = null,
            PagedResponseOffset<Movie> pagination = null
        );
        Task<List<Movie>> GetMoviesWithScreeningsForDayAsync(
            DateTime date,
            string sort,
            string genre = null
        );
        Task<Movie> AddMovieAsync(Movie movie);
        Task DeleteMovieAsync(long id);
        Task<List<Movie>> GetMoviesWithIds(List<long> ids);
        Task<bool> MovieExistsAsync(string movieName);
    }
}
