using DOMAIN.DTOs;
using DOMAIN.Entities;
using DOMAIN.ValueEntities;

namespace BLS.Interfaces
{
    public interface IGenreRepository
    {
        Task<Genre> GetGenreByIdAsync(long id);
        Task<Genre> GetGenreByNameAsync(string genreName);
        Task<PagedResponseOffset<Genre>> GetGenresAsync(
            List<string> startLetter = null,
            string containsLetters = null,
            PagedResponseOffset<Genre> pagination = null
        );
        Task<List<Genre>> GetGenresWithIds(List<long> ids);
        Task<Genre> AddGenreAsync(Genre genre);
        Task DeleteGenreAsync(long id);
    }
}
