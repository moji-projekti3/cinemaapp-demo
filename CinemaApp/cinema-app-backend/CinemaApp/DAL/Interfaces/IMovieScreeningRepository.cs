using DOMAIN.Entities;
using DOMAIN.ValueEntities;

namespace DAL.Interfaces
{
    public interface IMovieScreeningRepository
    {
        Task<MovieScreening> GetMovieScreeningByIdAsync(long id);
        Task<List<MovieScreening>> GetScreeningsByMovieIdAsync(long movieId);
        
        Task<PagedResponseOffset<MovieScreening>> GetMovieScreeningsAsync(
            List<string> startLetter = null,
            string containsLetters = null,
            PagedResponseOffset<MovieScreening> pagination = null
        );
        Task<MovieScreening> AddCinemaRepertoireAsync(MovieScreening movieScreening);
        Task DeleteMovieScreeningAsync(long id);
    }
}
