using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DOMAIN.Entities;

namespace DAL.Interfaces
{
    public interface ITicketReservationRepository
    {
        Task<TicketReservation> GetReservationByIdAsync(long id);
        Task<TicketReservation> ReserveTicketsAsync(TicketReservation ticketReservation);
        Task<List<TicketReservation>> GetReservationByScreeningIdAsync(long id);
        Task<List<TicketReservation>> GetUsersReservations(
            long userId,
            bool getCurrentReservations
        );
        Task<List<Seat>> GetSeatsWithIds(List<long> ids);
        Task CancelReservation(long id);
        Task CancelReservationsForMovieScreening(long screeningId);
    }
}
