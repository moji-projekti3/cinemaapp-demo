namespace DAL.Constants
{
    public static class Constants
    {
        public const string ContentRootPathForImages =
            "/Users/raca/Desktop/CinemaApp-demo/CinemaApp/cinema-app-backend/cinemaapp/";

        public static class Sort
        {
            public const string Alphabetically = "Alphabetically";
            public const string Chronologically = "Chronologically";
        }
    }
}
