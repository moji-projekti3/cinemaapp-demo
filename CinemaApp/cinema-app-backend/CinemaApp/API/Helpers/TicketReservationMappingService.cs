using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLS.Services;
using DOMAIN.DTOs;
using DOMAIN.Entities;
using DOMAIN.TicketReservationDTOs.DTOs;

namespace API.Helpers
{
    public class TicketReservationMappingService : Profile
    {
        public TicketReservationMappingService()
        {
            CreateMap<TicketReservationInputDTO, TicketReservation>()
                .ForMember(
                    dest => dest.ReservedSeats,
                    opt =>
                        opt.MapFrom(src => src.ReservedSeats.Select(id => new Seat() { Id = id }))
                );

            CreateMap<TicketReservation, ReservationDisplayDTO>();
            CreateMap<Seat, SeatDisplayDTO>();
        }
    }
}
