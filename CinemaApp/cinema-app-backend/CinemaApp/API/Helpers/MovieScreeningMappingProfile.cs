using AutoMapper;
using DOMAIN.DTOs;
using DOMAIN.DTOs.MovieScreeningDTOs;
using DOMAIN.Entities;
using DOMAIN.MoviscreeningDTOs.DTOs;
using DOMAIN.ValueEntities;

namespace API.Helpers
{
    public class MovieScreeningMappingProfile : Profile
    {
        public MovieScreeningMappingProfile()
        {
            CreateMap<MovieScreening, MovieScreeningDisplayDTO>()
                .ForMember(dest => dest.MovieName, opt => opt.MapFrom(src => src.Movie.Name));

            CreateMap<MovieScreening, MovieScreeningDisplayDTOForUserReservationsDisplay>()
                .ForMember(dest => dest.MovieName, opt => opt.MapFrom(src => src.Movie.Name))
                .ForMember(
                    dest => dest.MoviePosterImage,
                    opt => opt.MapFrom(src => src.Movie.PosterImage)
                );

            CreateMap<MovieScreeningInputDTO, MovieScreening>()
                .ForMember(
                    dest => dest.Seats,
                    opt =>
                        opt.MapFrom(src =>
                            MovieScreening.InitializeSeats(src.SeatRows, src.SeatColumns)
                        )
                )
                .ForMember(
                    dest => dest.Movie,
                    opt => opt.MapFrom(src => new Movie { Id = src.MovieId })
                );

            CreateMap<MovieScreeningUpdateDTO, MovieScreening>();

            CreateMap<
                PagedResponseOffsetDTO<MovieScreeningDisplayDTO>,
                PagedResponseOffset<MovieScreening>
            >();

            CreateMap<
                PagedResponseOffset<MovieScreening>,
                PagedResponseOffsetDTO<MovieScreeningDisplayDTO>
            >();
        }
    }
}
