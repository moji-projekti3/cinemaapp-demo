using AutoMapper;
using DOMAIN.DTOs;
using DOMAIN.Entities;
using DOMAIN.MovieDTOs.DTOs;
using DOMAIN.MoviscreeningDTOs.DTOs;
using DOMAIN.ValueEntities;

namespace API.Helpers
{
    public class MovieMappingProfile : Profile
    {
        public MovieMappingProfile()
        {
            CreateMap<MovieInputDTO, Movie>()
                .ForMember(
                    dest => dest.Genres,
                    act => act.MapFrom(src => src.GenreIds.Select(id => new Genre() { Id = id }))
                );
            CreateMap<Movie, MoviesDisplayDTO>();
            CreateMap<Movie, CinemaRepertoireDisplayDTO>()
                .ForMember(
                    dest => dest.Genres,
                    opt => opt.MapFrom(src => src.Genres.Select(g => g.Name).ToList())
                )
                .ForMember(
                    dest => dest.MovieScreenings,
                    opt =>
                        opt.MapFrom(src =>
                            src.MovieScreenings.Select(ms => new MovieScreeningBasicDTO
                            {
                                Id = ms.Id,
                                Date = ms.Date
                            })
                                .ToList()
                        )
                );
            CreateMap<PagedResponseOffsetDTO<MoviesDisplayDTO>, PagedResponseOffset<Movie>>();
            CreateMap<PagedResponseOffset<Movie>, PagedResponseOffsetDTO<MoviesDisplayDTO>>();
        }
    }
}
