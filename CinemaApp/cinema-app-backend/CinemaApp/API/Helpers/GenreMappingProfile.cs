using AutoMapper;
using DOMAIN.DTOs;
using DOMAIN.Entities;
using DOMAIN.GenreDTOs.DTOs;
using DOMAIN.ValueEntities;

namespace API.Helpers
{
    public class GenreMappingProfile : Profile
    {
        public GenreMappingProfile()
        {
            CreateMap<GenreInputDTO, Genre>();
            CreateMap<Genre, GenreDisplayDTO>();
            CreateMap<PagedResponseOffsetDTO<GenreDisplayDTO>, PagedResponseOffset<Genre>>();
            CreateMap<PagedResponseOffset<Genre>, PagedResponseOffsetDTO<GenreDisplayDTO>>();
        }
    }
}
