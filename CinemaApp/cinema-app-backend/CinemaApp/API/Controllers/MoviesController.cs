using API.Middleware;
using BLS.Services;
using DOMAIN.DTOs;
using DOMAIN.MovieDTOs.DTOs;
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;
using static DAL.Constants.Constants;
using FluentValidationResult = FluentValidation.Results.ValidationResult;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MoviesController : ControllerBase
    {
        private readonly MoviesService _service;
        private readonly IValidator<MovieInputDTO> _validator;

        public MoviesController(MoviesService service, IValidator<MovieInputDTO> validator)
        {
            _service = service;
            _validator = validator;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<MoviesDisplayDTO>> GetMovie(long id)
        {
            return Ok(await _service.GetMovieById(id));
        }

        [HttpGet("{movieId}/poster-image")]
        public async Task<IActionResult> GetMovieImage(long movieId)
        {
            var (imageStream, extension) = await _service.GetPosterImageAsync(movieId);
            if (imageStream == null)
            {
                return NotFound();
            }
            return File(imageStream, $"image/{extension}");
        }

        [HttpGet("repertoire")]
        public async Task<ActionResult<List<CinemaRepertoireDisplayDTO>>> GetScreeningsByDate(
            [FromQuery, BindRequired] DateTime date,
            [FromQuery] string genre = null,
            [FromQuery] string sort = null
        )
        {
            var screenings = await _service.GetCinemaRepertoireByDateAsync(date, sort, genre);
            return Ok(screenings);
        }

        [Authorize]
        [HttpGet]
        public async Task<ActionResult<PagedResponseOffsetDTO<MoviesDisplayDTO>>> GetMovies(
            [FromQuery] List<string> startLetter,
            [FromQuery] List<string> containsLetters,
            [FromQuery] int pageNumber,
            [FromQuery] int pageSize
        )
        {
            var paginationParameters = new PagedResponseOffsetDTO<MoviesDisplayDTO>
            {
                PageNumber = pageNumber,
                PageSize = pageSize
            };
            var movies = await _service.GetAllMovies(
                startLetter,
                containsLetters,
                paginationParameters
            );
            return Ok(movies);
        }

        [HttpPost]
        [FileValidationFilterMiddleware(new string[] { ".jpeg", ".jpg", ".png" }, 1024 * 1024)]
        public async Task<ActionResult<MoviesDisplayDTO>> AddMovie(
            [FromForm] string movie,
            [FromForm] IFormFile posterImage
        )
        {
            MovieInputDTO movieDTO = JsonConvert.DeserializeObject<MovieInputDTO>(movie);

            FluentValidationResult result = await _validator.ValidateAsync(movieDTO);
            if (!result.IsValid)
            {
                result.AddToModelState(this.ModelState);
                return ValidationProblem(this.ModelState);
            }

            var createdMovie = await _service.AddMovie(movieDTO, posterImage);
            return CreatedAtAction(nameof(GetMovie), new { id = createdMovie.Id }, createdMovie);
        }

        [HttpPut("{id}")]
        [FileValidationFilterMiddleware(new string[] { ".jpeg", ".jpg", ".png" }, 1024 * 1024)]
        public async Task<ActionResult<MoviesDisplayDTO>> UpdateMovie(
            long id,
            [FromForm] string movie,
            [FromForm] IFormFile posterImage
        )
        {
            MovieInputDTO movieDTO = JsonConvert.DeserializeObject<MovieInputDTO>(movie);

            FluentValidationResult result = await _validator.ValidateAsync(movieDTO);
            if (!result.IsValid)
            {
                result.AddToModelState(this.ModelState);
                return ValidationProblem(this.ModelState);
            }
            return Ok(await _service.UpdateMovie(movieDTO, posterImage, id));
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteMovie(long id)
        {
            await _service.DeleteMovie(id);
            return NoContent();
        }
    }
}
