using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using DAL.Data;
using DOMAIN.DTOs;
using DOMAIN.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace API.Controllers
{
    [AllowAnonymous]
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IConfiguration _configuration;
        private readonly CinemaContext _context;

        public AuthenticationController(
            UserManager<ApplicationUser> userManager,
            IConfiguration configuration,
            CinemaContext context
        )
        {
            this.userManager = userManager;
            _context = context;
            _configuration = configuration;
        }

        [HttpPost]
        [Route("login")]
        public IActionResult Login([FromBody] LoginDTO model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Login parameters invalid.");
            }

            var user = userManager.FindByNameAsync(model.Email).GetAwaiter().GetResult();
            if (
                user != null
                && userManager.CheckPasswordAsync(user, model.Password).GetAwaiter().GetResult()
            )
            {
                Console.WriteLine($"User Role: {user.UsersRole}");

                var authClaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Role, user.UsersRole.ToString()),
                    new Claim(ClaimTypes.Name, user.Email),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                };

                var authSigningKey = new SymmetricSecurityKey(
                    Encoding.UTF8.GetBytes(_configuration["Jwt:Key"])
                );

                var token = new JwtSecurityToken(
                    issuer: _configuration["Jwt:Issuer"],
                    audience: _configuration["Jwt:Audience"],
                    expires: DateTime.Now.AddHours(1),
                    claims: authClaims,
                    signingCredentials: new SigningCredentials(
                        authSigningKey,
                        SecurityAlgorithms.HmacSha256
                    )
                );

                return Ok(
                    new TokenDTO
                    {
                        UsersRole = user.UsersRole,
                        Email = user.Email,
                        Token = new JwtSecurityTokenHandler().WriteToken(token),
                        Expiration = token.ValidTo
                    }
                );
            }
            return BadRequest();
        }

        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register([FromBody] RegistrationDTO model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Registration parameters invalid.");
            }

            var userExists = await userManager.FindByEmailAsync(model.Email);
            if (userExists != null)
            {
                return BadRequest("User already exists");
            }

            ApplicationUser user = new ApplicationUser()
            {
                Email = model.Email,
                UserName = model.Email,
                Name = model.Name,
                DateOfBirth = model.DateOfBirth,
                UsersRole = model.UsersRole ?? UsersRole.Customer,
            };

            var result = await userManager.CreateAsync(user, model.Password);
            if (!result.Succeeded)
            {
                var errors = result.Errors.Select(e => e.Description);
                return BadRequest(
                    new
                    {
                        message = "Validation failed! Please check user details and try again.",
                        errors
                    }
                );
            }

            var customer = new User
            {
                Email = user.Email,
                Name = user.Name,
                DateOfBirth = user.DateOfBirth,
                UsersRole = user.UsersRole,
                IsAuthenticated = true,
            };

            _context.Users.Add(customer);
            await _context.SaveChangesAsync();

            return Ok();
        }
    }
}
