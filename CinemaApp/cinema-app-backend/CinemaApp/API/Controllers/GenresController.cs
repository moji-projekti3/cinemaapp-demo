using BLS.Services;
using DOMAIN.DTOs;
using DOMAIN.Entities;
using DOMAIN.GenreDTOs.DTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class GenresController : ControllerBase
    {
        private readonly GenreService _service;
        private readonly ILogger<GenresController> _logger;

        public GenresController(GenreService service, ILogger<GenresController> logger)
        {
            _service = service;
            _logger = logger;
        }

        [HttpGet("{id:long}")]
        public async Task<ActionResult<GenreDisplayDTO>> GetGenre(long id)
        {
            return Ok(await _service.GetGenreById(id));
        }

        [HttpGet]
        public async Task<ActionResult<PagedResponseOffsetDTO<GenreDisplayDTO>>> GetAllGenres(
            [FromQuery] List<string> startLetter,
            [FromQuery] string containsLetters,
            [FromQuery] int pageNumber,
            [FromQuery] int pageSize
        )
        {
            var paginationParameters = new PagedResponseOffsetDTO<GenreDisplayDTO>
            {
                PageNumber = pageNumber,
                PageSize = pageSize
            };
            var genres = await _service.GetAllGenres(
                startLetter,
                containsLetters,
                paginationParameters
            );
            return Ok(genres);
        }
        [Authorize (Roles = nameof(UsersRole.Admin))]
        [HttpPost]
        public async Task<ActionResult<GenreDisplayDTO>> CreateGenre(GenreInputDTO genre)
        {
            var newGenreDto = await _service.AddGenre(genre);
            return CreatedAtAction(nameof(GetGenre), new { id = newGenreDto.Id }, newGenreDto);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<GenreDisplayDTO>> UpdateGenre(
            [FromBody] GenreInputDTO genreUpdate,
            long id
        )
        {
            return Ok(await _service.UpdateGenre(genreUpdate, id));
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteGenre(long id)
        {
            await _service.DeleteGenre(id);
            return NoContent();
        }
    }
}
