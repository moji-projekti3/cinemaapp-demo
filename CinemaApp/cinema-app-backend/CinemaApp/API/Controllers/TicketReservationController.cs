using BLS.Services;
using DOMAIN.DTOs;
using DOMAIN.TicketReservationDTOs.DTOs;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TicketReservationController : ControllerBase
    {
        private readonly TicketReservationService service;

        public TicketReservationController(TicketReservationService service)
        {
            this.service = service;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<MoviesDisplayDTO>> GetReservationById(long id)
        {
            return Ok(await service.GetReservationById(id));
        }

        [HttpGet]
        public async Task<ActionResult<ReservationDisplayDTO>> GetUsersReservations(
            [FromQuery] long userId,
            [FromQuery] bool currentReservations
        )
        {
            return Ok(await service.GetUsersReservationsByUserIdAsync(userId, currentReservations));
        }

        [HttpPost]
        public async Task<ActionResult<ReservationDisplayDTO>> CreateReservation(
            TicketReservationInputDTO ticketReservationInputDTO
        )
        {
            var createdReservation = await service.MakeReservationAsync(ticketReservationInputDTO);
            return CreatedAtAction(
                nameof(GetReservationById),
                new { id = createdReservation.Id },
                createdReservation
            );
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> CancelReservation(long id)
        {
            await service.CancelReservationAsync(id);
            return NoContent();
        }
    }
}
