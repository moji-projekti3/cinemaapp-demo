using BLS.Services;
using DOMAIN.DTOs;
using DOMAIN.Entities;
using DOMAIN.MoviscreeningDTOs.DTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MovieScreeningsController : ControllerBase
    {
        private readonly MovieScreeningService movieScreeningService;

        public MovieScreeningsController(MovieScreeningService movieScreeningService)
        {
            this.movieScreeningService = movieScreeningService;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<MovieScreeningDisplayDTO>> GetMovieScreening(long id)
        {
            return Ok(await movieScreeningService.GetScreeningById(id));
        }
        [Authorize (Roles =nameof (UsersRole.Admin))]
        [HttpPost]
        public async Task<ActionResult<MovieScreeningDisplayDTO>> AddMovieScreening(
            MovieScreeningInputDTO repertoireInputDTO
        )
        {
            var screening = await movieScreeningService.AddMovieScreening(repertoireInputDTO);
            return CreatedAtAction(nameof(GetMovieScreening), new { id = screening.Id }, screening);
        }

        [HttpGet]
        public async Task<
            ActionResult<PagedResponseOffsetDTO<MovieScreeningDisplayDTO>>
        > GetRepertoires(
            [FromQuery] List<string> startLetter,
            [FromQuery] string containsLetters,
            [FromQuery] int pageNumber,
            [FromQuery] int pageSize
        )
        {
            var paginationParameters = new PagedResponseOffsetDTO<MovieScreeningDisplayDTO>
            {
                PageNumber = pageNumber,
                PageSize = pageSize
            };
            var movieScreenings = await movieScreeningService.GetAllMovieScreenings(
                startLetter,
                containsLetters,
                paginationParameters
            );
            return Ok(movieScreenings);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<MovieScreeningDisplayDTO>> UpdateMovie(
            long id,
            [FromBody] MovieScreeningUpdateDTO movieScreeningUpdate
        )
        {
            var updatedMovieScreeningDTO = await movieScreeningService.UpdateMovieScreening(
                movieScreeningUpdate,
                id
            );
            return Ok(updatedMovieScreeningDTO);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteMovieScreening(long id)
        {
            await movieScreeningService.DeleteMovieScreening(id);
            return NoContent();
        }
    }
}
