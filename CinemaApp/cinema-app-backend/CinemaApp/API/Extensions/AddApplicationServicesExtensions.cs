using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLS.Interfaces;
using BLS.Services;
using DAL.Data;
using DAL.Data.Repository;
using DAL.Interfaces;
using DOMAIN.Validation;
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.EntityFrameworkCore;

namespace API.Extensions
{
    public static class AddApplicationCustomServicesExtensions
    {
        public static IServiceCollection AddApplicationDataServices(
            this IServiceCollection services,
            IConfiguration configuration
        )
        {
            services.AddScoped<IGenreRepository, GenresRepository>();
            services.AddScoped<GenreService>();
            services.AddScoped<IMoviesRepository, MoviesRepository>();
            services.AddScoped<MoviesService>();
            services.AddScoped<IMovieScreeningRepository, MovieScreeningsRepository>();
            services.AddScoped<MovieScreeningService>();
            services.AddScoped<ITicketReservationRepository, TicketReservationRepository>();
            services.AddScoped<TicketReservationService>();
            services.AddScoped<IUserRepository, UsersRepository>();
            services.AddScoped<UserService>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            return services;
        }
    }
}
