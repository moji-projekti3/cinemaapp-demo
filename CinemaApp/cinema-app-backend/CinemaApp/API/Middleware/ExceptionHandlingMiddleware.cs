using System.Net;
using System.Text.Json;
using DOMAIN.Exceptions;
using DOMAIN.Exceptions.GenreExceptions;

namespace API.Middleware
{
    public class ExceptionHandlingMiddleware
    {
        private RequestDelegate requestDelegate;
        private readonly ILogger<ExceptionHandlingMiddleware> logger;

        public ExceptionHandlingMiddleware(
            RequestDelegate requestDelegate,
            ILogger<ExceptionHandlingMiddleware> logger
        )
        {
            this.logger = logger;
            this.requestDelegate = requestDelegate;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                logger.LogInformation("");
                await requestDelegate(context);
            }
            catch (Exception ex)
            {
                logger.LogError(
                    ex,
                    "An exception occurred for the request {RequestMethod} {RequestPath}",
                    context.Request.Method,
                    context.Request.Path
                );
                await HandleException(context, ex);
            }
        }

        private static Task HandleException(HttpContext context, Exception ex)
        {
            var errorMessageObject = new Error { Message = ex.Message };

            var statusCode = (int)HttpStatusCode.InternalServerError;

            statusCode = ex switch
            {
                GenreNotFoundException
                or MovieNotFoundException
                or NoImageToReadException
                or MovieScreeningNotFoundException
                or GenreIsDeletedException
                or ReservationNotFoundException
                or UserNotFoundException
                    => (int)HttpStatusCode.NotFound,
                GenreAlreadyExistsException
                or SeatsAreAlreadyReservedException
                or TimeOverplapException
                    => (int)HttpStatusCode.Conflict,
                InvalidIdValue
                or EnteredReperoireDateHasPassed
                or FileSizeIsNotWithinLimitException
                    => (int)HttpStatusCode.BadRequest,
                _ => (int)HttpStatusCode.InternalServerError,
            };
            var errorMessage = JsonSerializer.Serialize(errorMessageObject);

            context.Response.ContentType = "application/json";

            context.Response.StatusCode = statusCode;

            return context.Response.WriteAsync(errorMessage);
        }
    }
}
