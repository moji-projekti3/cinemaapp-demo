using DOMAIN.Validation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace API.Middleware
{
    public class FileValidationFilterMiddleware : ActionFilterAttribute
    {
        private readonly string[] allowedExtensions;
        private readonly long maxSize;

        public FileValidationFilterMiddleware(string[] allowedExtensions, long maxSize)
        {
            this.allowedExtensions = allowedExtensions;
            this.maxSize = maxSize;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var param = context.ActionArguments.SingleOrDefault(p => p.Value is IFormFile);

            if (param.Value is IFormFile file)
            {
                if (file.Length == 0)
                {
                    context.Result = new BadRequestObjectResult("File is null but was expected.");
                    return;
                }

                if (!FileValidator.IsFileExtensionAllowed(file, allowedExtensions))
                {
                    var allowedExtensionsMessage = String
                        .Join(", ", allowedExtensions)
                        .Replace(".", "")
                        .ToUpper();
                    context.Result = new BadRequestObjectResult(
                        "Invalid file type. " + $"Please upload {allowedExtensionsMessage} file."
                    );
                    return;
                }

                if (!FileValidator.IsFileSizeWithinLimit(file, maxSize))
                {
                    var mbSize = (double)maxSize / 1024 / 1024;
                    context.Result = new BadRequestObjectResult(
                        $"File size exceeds the maximum allowed size ({mbSize} MB)."
                    );
                    return;
                }
            }
            base.OnActionExecuting(context);
        }
    }
}
