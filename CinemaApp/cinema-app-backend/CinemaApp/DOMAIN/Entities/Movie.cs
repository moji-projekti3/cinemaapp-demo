using Microsoft.AspNetCore.Http;

namespace DOMAIN.Entities
{
    public class Movie : BaseEntity
    {
        public string PosterImage { get; set; }
        public string Name { get; set; }
        public string OriginalName { get; set; }
        public int Duration { get; set; }
        public bool IsDeleted { get; set; } = false;
        public List<Genre> Genres { get; set; }
        public List<MovieScreening> MovieScreenings { get; set; }
    }
}
