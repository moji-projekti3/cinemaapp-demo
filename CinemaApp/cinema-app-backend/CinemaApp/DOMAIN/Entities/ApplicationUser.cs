using Microsoft.AspNetCore.Identity;

namespace DOMAIN.Entities
{
    public class ApplicationUser : IdentityUser
    {
        public string Name { get; set; }
        public DateTime DateOfBirth { get; set; }
        public UsersRole UsersRole { get; set; }
    }
}
