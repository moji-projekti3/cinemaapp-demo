namespace DOMAIN.Entities
{
    public enum UsersRole
    {
        Admin,
        Customer,
    }

    public class User : BaseEntity
    {
        public string Name { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Email { get; set; }
        public bool IsAuthenticated { get; set; } = false;
        public UsersRole UsersRole { get; set; }
        public List<TicketReservation> Reservations { get; set; }
    }
}
