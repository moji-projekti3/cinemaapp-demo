using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DOMAIN.MoviscreeningDTOs.DTOs;

namespace DOMAIN.Entities
{
    public class MovieScreening : BaseEntity
    {
        public DateTime Date { get; set; }
        public decimal TicketPrice { get; set; }
        public bool IsDeleted { get; set; } = false;
        public Movie Movie { get; set; }
        public List<Seat> Seats { get; set; } = new List<Seat>();

        public static List<Seat> InitializeSeats(int rows, int columns)
        {
            List<Seat> seats = new List<Seat>(rows * columns);

            for (int row = 1; row <= rows; row++)
            {
                for (int column = 1; column <= columns; column++)
                {
                    seats.Add(
                        new Seat
                        {
                            Row = row,
                            Column = column,
                            IsReserved = false
                        }
                    );
                }
            }

            return seats;
        }
    }
}
