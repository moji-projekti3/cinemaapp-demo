
namespace DOMAIN.Entities
{
    public class Seat : BaseEntity
    {
        public MovieScreening Screening { get; set; }
        public int Row { get; set; }
        public int Column { get; set; }
        public bool IsReserved { get; set; } = false;
    }
}
