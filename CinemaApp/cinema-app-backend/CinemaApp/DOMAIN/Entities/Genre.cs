namespace DOMAIN.Entities
{
    public class Genre : BaseEntity
    {
        public string Name { get; set; }
        public List<Movie> Movies { get; set; }
        public bool IsDeleted { get; set; } = false;
    }
}
