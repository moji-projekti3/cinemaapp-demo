using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DOMAIN.Entities
{
    public class TicketReservation : BaseEntity
    {
        public string UserEmail { get; set; }
        public User User { get; set; }
        public MovieScreening MovieScreening { get; set; }
        public List<Seat> ReservedSeats { get; set; } = new List<Seat>();
        public decimal TotalPrice { get; set; }
        public bool IsCancelled { get; set; } = false;
    }
}
