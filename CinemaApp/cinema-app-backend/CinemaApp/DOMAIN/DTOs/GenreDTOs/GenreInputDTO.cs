namespace DOMAIN.DTOs
{
    public class GenreInputDTO
    {
        public string Name { get; set; }
    }
}
