namespace DOMAIN.GenreDTOs.DTOs
{
    public class GenreDisplayDTO
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
