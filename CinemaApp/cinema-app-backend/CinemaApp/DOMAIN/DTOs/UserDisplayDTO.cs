using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DOMAIN.Entities;

namespace DOMAIN.DTOs
{
    public class UserDisplayDTO
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public UsersRole UsersRole { get; set; }
    }
}
