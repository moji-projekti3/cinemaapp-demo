using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DOMAIN.Entities;

namespace DOMAIN.DTOs
{
    public class RegistrationDTO
    {
        [EmailAddress]
        [Required(ErrorMessage = "Email is required")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [RegularExpression("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[^\\da-zA-Z]).{8,15}$")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Users first name is required")]
        [StringLength(50)]
        public string Name { get; set; }
        public UsersRole? UsersRole { get; set; }

        [Required(ErrorMessage = "User's birthday is required")]
        public DateTime DateOfBirth { get; set; }
    }
}
