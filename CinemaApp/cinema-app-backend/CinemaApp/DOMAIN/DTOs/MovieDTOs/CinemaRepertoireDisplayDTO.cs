using DOMAIN.MoviscreeningDTOs.DTOs;

namespace DOMAIN.DTOs
{
    public class CinemaRepertoireDisplayDTO
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string OriginalName { get; set; }
        public string PosterImage { get; set; }
        public int Duration { get; set; }
        public List<string> Genres { get; set; }
        public List<MovieScreeningBasicDTO> MovieScreenings { get; set; }
    }
}
