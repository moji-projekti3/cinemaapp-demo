using DOMAIN.Entities;
using DOMAIN.GenreDTOs.DTOs;

namespace DOMAIN.DTOs
{
    public class MoviesDisplayDTO
    {
        public long Id { get; set; }
        public string PosterImage { get; set; }
        public string Name { get; set; }
        public string OriginalName { get; set; }
        public int Duration { get; set; }
        public List<GenreDisplayDTO> Genres { get; set; }
    }
}
