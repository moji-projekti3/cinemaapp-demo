using DOMAIN.Entities;
using Microsoft.AspNetCore.Http;

namespace DOMAIN.MovieDTOs.DTOs
{
    public class MovieInputDTO 
    {
        public string Name { get; set; }
        public string OriginalName { get; set; }
        public int Duration { get; set; }
        public List<long> GenreIds { get; set; }
    }
}
