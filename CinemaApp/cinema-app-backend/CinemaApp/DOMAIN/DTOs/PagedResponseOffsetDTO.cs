using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DOMAIN.DTOs
{
    public class PagedResponseOffsetDTO<T>
    {
        public int PageNumber { get; init; }
        public int PageSize { get; init; }
        public int? TotalPages { get; init; }
        public int? TotalRecords { get; init; }
        public List<T> Data { get; init; }
    }
}
