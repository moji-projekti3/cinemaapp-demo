using DOMAIN.DTOs;
using DOMAIN.DTOs.MovieScreeningDTOs;
using DOMAIN.MoviscreeningDTOs.DTOs;

namespace DOMAIN.TicketReservationDTOs.DTOs
{
    public class ReservationDisplayDTO
    {
        public long Id { get; set; }
        public string UserEmail { get; set; }
        public MovieScreeningDisplayDTOForUserReservationsDisplay MovieScreening { get; set; }
        public List<SeatDisplayDTO> ReservedSeats { get; set; }
        public decimal TotalPrice { get; set; }
    }
}
