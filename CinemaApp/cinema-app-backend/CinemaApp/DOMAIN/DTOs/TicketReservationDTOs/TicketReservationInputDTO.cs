using System.ComponentModel.DataAnnotations;

namespace DOMAIN.DTOs
{
    public class TicketReservationInputDTO
    {
        public long MovieScreeningId { get; set; }
        public long UserId { get; set; }
        public string UserEmail { get; set; }
        public List<long> ReservedSeats { get; set; }
    }
}
