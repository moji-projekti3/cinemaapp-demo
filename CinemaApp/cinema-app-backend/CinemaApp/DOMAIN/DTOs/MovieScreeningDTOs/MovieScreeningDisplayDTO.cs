using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DOMAIN.DTOs;

namespace DOMAIN.MoviscreeningDTOs.DTOs
{
    public class MovieScreeningDisplayDTO
    {
        public long Id { get; set; }
        public DateTime Date { get; set; }
        public decimal TicketPrice { get; set; }
        public string MovieName { get; set; }
        public List<SeatDisplayDTO> Seats { get; set; }
    }
}
