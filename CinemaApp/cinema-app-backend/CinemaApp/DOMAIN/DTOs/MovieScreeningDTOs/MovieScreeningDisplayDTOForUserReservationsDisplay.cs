using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DOMAIN.DTOs.MovieScreeningDTOs
{
    public class MovieScreeningDisplayDTOForUserReservationsDisplay
    {
        public long Id { get; set; }
        public DateTime Date { get; set; }
        public string MovieName { get; set; }
        public string MoviePosterImage { get; set; }
    }
}
