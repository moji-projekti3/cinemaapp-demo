using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DOMAIN.MoviscreeningDTOs.DTOs
{
    public class MovieScreeningBasicDTO
    {
        public long Id { get; set; }
        public DateTime Date { get; set; }
    }
}
