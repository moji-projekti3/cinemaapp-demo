using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DOMAIN.MoviscreeningDTOs.DTOs
{
    public class MovieScreeningInputDTO
    {
        public DateTime Date { get; set; }
        public decimal TicketPrice { get; set; }
        public int SeatRows { get; set; }
        public int SeatColumns { get; set; }
        public long MovieId { get; set; }
    }
}
