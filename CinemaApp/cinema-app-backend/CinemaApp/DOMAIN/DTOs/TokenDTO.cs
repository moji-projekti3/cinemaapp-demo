using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DOMAIN.Entities;

namespace DOMAIN.DTOs
{
    public class TokenDTO
    {
        public string Token { get; set; }
        public string Email { get; set; }
        public UsersRole UsersRole { get; set; }
        public DateTime Expiration { get; set; }
    }
}
