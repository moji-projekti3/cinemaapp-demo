using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DOMAIN.Exceptions
{
    public class SeatsAreAlreadyReservedException : Exception
    {
        public SeatsAreAlreadyReservedException(string message)
            : base($"Seat with ID {message} is already taken!") { }
    }
}
