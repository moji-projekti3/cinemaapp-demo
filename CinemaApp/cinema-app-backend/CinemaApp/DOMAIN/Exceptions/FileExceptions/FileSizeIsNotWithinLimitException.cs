using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DOMAIN.Exceptions
{
    public class FileSizeIsNotWithinLimitException : Exception
    {
        public FileSizeIsNotWithinLimitException(string message) : base($"File {message} is not within the size limit!")
        {
        }
    }
}