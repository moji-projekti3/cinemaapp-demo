using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DOMAIN.Exceptions
{
    public class NoImageToReadException : Exception
    {
        public NoImageToReadException(string message) : base($"No image to read, {message} is added.")
        {
        }
    }
}