using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DOMAIN.Exceptions
{
    public class FileExtensionNotAllowedException : Exception
    {
        public FileExtensionNotAllowedException(string message) : base($"Invalid file type {message}. Please upload a .png,.jpg or .jpeg file.")
        {
        }
    }
}