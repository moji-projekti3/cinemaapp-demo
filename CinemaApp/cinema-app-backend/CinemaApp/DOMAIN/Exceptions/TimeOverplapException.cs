using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DOMAIN.Exceptions
{
    public class TimeOverplapException : Exception
    {
        public TimeOverplapException(string message)
            : base($"Time of screening you enterred {message} is not available!") { }
    }
}
