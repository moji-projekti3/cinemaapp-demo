using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DOMAIN.Exceptions
{
    public class ReservationNotFoundException : Exception
    {
        public ReservationNotFoundException(string message)
            : base($"Reservation with id {message} not found!") { }
    }
}
