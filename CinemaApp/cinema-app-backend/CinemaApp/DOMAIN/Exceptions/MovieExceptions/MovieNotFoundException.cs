using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DOMAIN.Exceptions
{
    public class MovieNotFoundException : Exception
    {
        public MovieNotFoundException(string message)
            : base($"Movie with ID {message} not found!") { }
    }
}
