using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DOMAIN.Exceptions
{
    public class MovieScreeningNotFoundException : Exception
    {
        public MovieScreeningNotFoundException(string message) : base($"Movie with ID {message} not found!")
        {
            
        }
    }
}