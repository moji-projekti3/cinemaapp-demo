using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DOMAIN.Exceptions
{
    public class MovieWithThatNameAlreadyExists : Exception
    {
        public MovieWithThatNameAlreadyExists(string message) : base($"Movie with name {message} already exists!")
        {
        }
    }
}