using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DOMAIN.Exceptions
{
    public class EnteredReperoireDateHasPassed : Exception
    {
        public EnteredReperoireDateHasPassed(string message)
            : base(
                $"Date {message} you entered is not valid, please choose the date in range of seven days from {DateTime.Now}."
            ) { }
    }
}
