using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DOMAIN.Exceptions
{
    public class GenreAlreadyExistsException : Exception
    {
        public GenreAlreadyExistsException(string genreName)
            : base($"Genre with name {genreName} already exists!") { }
    }
}
