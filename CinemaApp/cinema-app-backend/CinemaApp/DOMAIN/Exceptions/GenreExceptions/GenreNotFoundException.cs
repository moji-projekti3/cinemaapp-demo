using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DOMAIN.Exceptions
{
    public class GenreNotFoundException : Exception
    {
        public GenreNotFoundException(string message)
            : base($"Genre with ID {message} not found!") { }
    }
}
