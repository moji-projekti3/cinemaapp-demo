using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DOMAIN.Exceptions
{
    public class InvalidIdValue : Exception
    {
        public InvalidIdValue(string message)
            : base($"{message} is not valid ID value!") { }
    }
}
