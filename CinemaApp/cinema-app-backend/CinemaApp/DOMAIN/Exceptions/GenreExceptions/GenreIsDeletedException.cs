using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DOMAIN.Exceptions.GenreExceptions
{
    public class GenreIsDeletedException : Exception
    {
        public GenreIsDeletedException(string message)
            : base($"Genre named {message} is deleted!") { }
    }
}
