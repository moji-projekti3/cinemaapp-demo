namespace DOMAIN.ValueEntities
{
    public record PagedResponseOffset<T>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int TotalRecords { get; set; }
        public int TotalPages { get; set; }
        public List<T> Data { get; set; }

        public PagedResponseOffset(
            List<T> data,
            int pageNumber,
            int pageSize,
            int totalRecords,
            int totalPages
        )
        {
            Data = data;
            PageNumber = pageNumber;
            PageSize = pageSize;
            TotalRecords = totalRecords;
            TotalPages = totalPages;
        }
    }
}
