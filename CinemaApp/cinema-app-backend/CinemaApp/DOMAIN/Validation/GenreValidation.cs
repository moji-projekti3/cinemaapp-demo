using DOMAIN.DTOs;
using FluentValidation;

namespace DOMAIN.Validation
{
    public class GenreValidation : AbstractValidator<GenreInputDTO>
    {
        public GenreValidation()
        {
            RuleFor(genre => genre.Name).NotEmpty().MaximumLength(20);
        }
    }
}
