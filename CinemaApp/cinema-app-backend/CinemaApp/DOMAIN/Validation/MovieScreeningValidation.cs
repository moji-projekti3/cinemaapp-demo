using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using DOMAIN.DTOs;
using DOMAIN.Entities;
using DOMAIN.MoviscreeningDTOs.DTOs;
using FluentValidation;

namespace DOMAIN.Validation
{
    public class MovieScreeningValidation : AbstractValidator<MovieScreeningInputDTO>
    {
        public MovieScreeningValidation()
        {
            RuleFor(repertoire => repertoire.Date).NotEmpty();
            RuleFor(repertoire => repertoire.SeatRows).NotNull().GreaterThan(0).LessThan(15);
            ;
            RuleFor(repertoire => repertoire.SeatColumns).NotNull().GreaterThan(0).LessThan(15);
            ;
            RuleFor(repertoire => repertoire.TicketPrice).NotNull().GreaterThan(0).LessThan(20);
            ;
            RuleFor(repertoire => repertoire.MovieId).NotNull();
        }
    }
}
