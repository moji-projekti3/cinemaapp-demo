using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using DOMAIN.DTOs;
using FluentValidation;

namespace DOMAIN.Validation
{
    public class TicketReservationValidation : AbstractValidator<TicketReservationInputDTO>
    {
        private const string EmailRegex = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}$";

        public TicketReservationValidation()
        {
            RuleFor(ticket => ticket.MovieScreeningId).NotNull();
            RuleFor(ticket => ticket.ReservedSeats).NotNull();
            RuleFor(ticket => ticket.UserEmail).NotEmpty().Matches(EmailRegex);
        }
    }
}
