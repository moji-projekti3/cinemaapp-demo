using DOMAIN.MovieDTOs.DTOs;
using FluentValidation;

namespace DOMAIN.Validation
{
    public class MovieValidation : AbstractValidator<MovieInputDTO>
    {
        public MovieValidation()
        {
            RuleFor(movie => movie.Name).NotEmpty().MaximumLength(40);
            RuleFor(movie => movie.OriginalName).NotEmpty().MaximumLength(45);
            RuleFor(movie => movie.Duration).NotNull().GreaterThan(0).LessThan(400);
            RuleFor(movie => movie.GenreIds).NotEmpty();
        }
    }
}
