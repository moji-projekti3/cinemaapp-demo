using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace DOMAIN.Validation
{
    public class FileValidator
    {
        public static bool IsFileExtensionAllowed(IFormFile file, params string[] allowedExtensions)
        {
            var extension = Path.GetExtension(file.FileName);
            return allowedExtensions.Contains(extension);
        }

        public static bool IsFileSizeWithinLimit(IFormFile file, long maxSizeInKb)
        {
            int bytesInKb = 1024;
            return file.Length <= maxSizeInKb * bytesInKb;
        }
        
    }
}
