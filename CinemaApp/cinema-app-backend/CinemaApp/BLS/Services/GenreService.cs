using AutoMapper;
using DAL.Interfaces;
using DOMAIN.DTOs;
using DOMAIN.Entities;
using DOMAIN.Exceptions;
using DOMAIN.Exceptions.GenreExceptions;
using DOMAIN.GenreDTOs.DTOs;
using DOMAIN.ValueEntities;
using Microsoft.Extensions.Logging;

namespace BLS.Services
{
    public class GenreService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper mapper;
        private readonly ILogger<GenreService> logger;

        public GenreService(IUnitOfWork unitOfWork, IMapper mapper, ILogger<GenreService> logger)
        {
            this.logger = logger;
            this.mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<GenreDisplayDTO> GetGenreById(long id)
        {
            if (id < 1)
            {
                throw new InvalidIdValue($"{id}");
            }
            var genre = await _unitOfWork.Genres.GetGenreByIdAsync(id) ?? throw new GenreNotFoundException($"{id}");
            if (genre.IsDeleted == true)
            {
                throw new GenreIsDeletedException(genre.Name);
            }
            return mapper.Map<GenreDisplayDTO>(genre);
        }

        public async Task<PagedResponseOffsetDTO<GenreDisplayDTO>> GetAllGenres(
            List<string> startLetter = null,
            string containsLetters = null,
            PagedResponseOffsetDTO<GenreDisplayDTO> cursorParameters = null
        )
        {
            var paginationParameters = mapper.Map<PagedResponseOffset<Genre>>(cursorParameters);
            var genresPagData = await _unitOfWork.Genres.GetGenresAsync(
                startLetter,
                containsLetters,
                paginationParameters
            );

            return mapper.Map<PagedResponseOffsetDTO<GenreDisplayDTO>>(genresPagData);
        }

        public async Task<GenreDisplayDTO> AddGenre(GenreInputDTO genreDto)
        {
            //provera za vec postojeci zanr sa tim imenom (ODRADJENO)
            var genre = mapper.Map<Genre>(genreDto);
            var findGenreWithSameName = await _unitOfWork.Genres.GetGenreByNameAsync(genreDto.Name);
            if (findGenreWithSameName == null)
            {
                await _unitOfWork.Genres.AddGenreAsync(genre);
                await _unitOfWork.Complete();
                return mapper.Map<GenreDisplayDTO>(genre);
            }
            else
            {
                throw new GenreAlreadyExistsException(genreDto.Name);
            }
        }

        public async Task<GenreDisplayDTO> UpdateGenre(GenreInputDTO genreUpdateDto, long id)
        {
            //provera za vec postojeci zanr sa tim imenom (ODRADJENO)
            var genre = await _unitOfWork.Genres.GetGenreByIdAsync(id);
            if (genre.IsDeleted == true)
            {
                throw new GenreIsDeletedException(genre.Name);
            }
            var findGenreWithSameName = await _unitOfWork.Genres.GetGenreByNameAsync(
                genreUpdateDto.Name
            );
            if (genre == null)
            {
                throw new GenreNotFoundException($"{id}");
            }
            else if (findGenreWithSameName != null)
            {
                throw new GenreAlreadyExistsException(genreUpdateDto.Name);
            }
            genre = mapper.Map(genreUpdateDto, genre);

            await _unitOfWork.Complete();
            return mapper.Map<GenreDisplayDTO>(genre);
        }

        public async Task DeleteGenre(long id)
        {
            var genre = await _unitOfWork.Genres.GetGenreByIdAsync(id) ?? throw new GenreNotFoundException($"{id}");
            await _unitOfWork.Genres.DeleteGenreAsync(id);
            await _unitOfWork.Complete();
        }
    }
}
