using AutoMapper;
using DAL.Data.Utils;
using DAL.Interfaces;
using DOMAIN.DTOs;
using DOMAIN.Entities;
using DOMAIN.Exceptions;
using DOMAIN.TicketReservationDTOs.DTOs;
using Microsoft.Extensions.Configuration;

namespace BLS.Services
{
    public class TicketReservationService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        private readonly IConfiguration configuration;

        public TicketReservationService(
            IUnitOfWork unitOfWork,
            IMapper mapper,
            IConfiguration configuration
        )
        {
            this.configuration = configuration;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        private static decimal CalculateTotalPrice(TicketReservation ticketReservation)
        {
            return ticketReservation.TotalPrice =
                ticketReservation.ReservedSeats.Count
                * ticketReservation.MovieScreening.TicketPrice;
        }

        public async Task CancelReservationAsync(long id)
        {
            _ =
                await unitOfWork.TicketReservations.GetReservationByIdAsync(id)
                ?? throw new ReservationNotFoundException(id.ToString());
            await unitOfWork.TicketReservations.CancelReservation(id);
            await unitOfWork.Complete();
        }

        public async Task<List<ReservationDisplayDTO>> GetUsersReservationsByUserIdAsync(
            long userId,
            bool currentReservations
        )
        {
            var user =
                await unitOfWork.UserRepository.GetUserById(userId)
                ?? throw new UserNotFoundException(userId.ToString());
            var reservations = await unitOfWork.TicketReservations.GetUsersReservations(
                userId,
                currentReservations
            );

            if (currentReservations == false)
            {
                var usersReservations = reservations.Where(r =>
                    r.User.Id == userId && r.MovieScreening.Date < DateTime.Today
                );
                var movieId = usersReservations
                    .Select(movie => movie.MovieScreening.Movie.Id)
                    .ToList();
                var movie = await unitOfWork.Movies.GetMoviesWithIds(movieId);
                GetPosterImages(movie.ToArray());
                return mapper.Map<List<ReservationDisplayDTO>>(usersReservations);
            }
            else
            {
                var usersReservations = reservations.Where(r =>
                    r.User.Id == userId && r.MovieScreening.Date >= DateTime.Today
                );
                var movieId = usersReservations
                    .Select(movie => movie.MovieScreening.Movie.Id)
                    .ToList();
                var movie = await unitOfWork.Movies.GetMoviesWithIds(movieId);
                GetPosterImages(movie.ToArray());
                return mapper.Map<List<ReservationDisplayDTO>>(usersReservations);
            }
        }

        public async Task<ReservationDisplayDTO> GetReservationById(long id)
        {
            var reservation =
                await unitOfWork.TicketReservations.GetReservationByIdAsync(id)
                ?? throw new ReservationNotFoundException(id.ToString());
            return mapper.Map<ReservationDisplayDTO>(reservation);
        }

        private static void SetSeatsAsReserved(List<Seat> reservedSeats)
        {
            foreach (var seat in reservedSeats)
            {
                if (seat.IsReserved == true)
                {
                    throw new SeatsAreAlreadyReservedException($"{seat.Id}");
                }
                seat.IsReserved = true;
            }
        }

        public async Task<ReservationDisplayDTO> MakeReservationAsync(
            TicketReservationInputDTO ticketReservationInputDTO
        )
        {
            var movieScreening =
                await unitOfWork.Screenings.GetMovieScreeningByIdAsync(
                    ticketReservationInputDTO.MovieScreeningId
                )
                ?? throw new MovieScreeningNotFoundException(
                    $"{ticketReservationInputDTO.MovieScreeningId}"
                );
            var seats = await unitOfWork.TicketReservations.GetSeatsWithIds(
                ticketReservationInputDTO.ReservedSeats
            );
            SetSeatsAsReserved(seats);
            var user = await unitOfWork.UserRepository.GetUserByEmail(
                ticketReservationInputDTO.UserEmail
            );
            var reservationInput = mapper.Map<TicketReservation>(ticketReservationInputDTO);
            reservationInput.User = user;
            reservationInput.ReservedSeats = seats;
            reservationInput.MovieScreening = movieScreening;
            reservationInput.TotalPrice = CalculateTotalPrice(reservationInput);
            var reservation = await unitOfWork.TicketReservations.ReserveTicketsAsync(
                reservationInput
            );
            await unitOfWork.Complete();
            return mapper.Map<ReservationDisplayDTO>(reservation);
        }

        public void GetPosterImages(params Movie[] movies)
        {
            string resourcesPath = configuration["BASE_URL"];
            string imagesPath = configuration["GET_IMAGES"];

            var imagePath = resourcesPath + imagesPath;
            foreach (var movie in movies)
            {
                movie.PosterImage = FilesUtil.GetPath(movie.PosterImage, imagePath);
            }
        }
    }
}
