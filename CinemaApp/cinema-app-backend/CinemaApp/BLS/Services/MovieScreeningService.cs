using AutoMapper;
using DAL.Interfaces;
using DOMAIN.DTOs;
using DOMAIN.Entities;
using DOMAIN.Exceptions;
using DOMAIN.MoviscreeningDTOs.DTOs;
using DOMAIN.ValueEntities;

namespace BLS.Services
{
    public class MovieScreeningService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public MovieScreeningService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public async Task<MovieScreeningDisplayDTO> GetScreeningById(long id)
        {
            var movieScreening =
                await unitOfWork.Screenings.GetMovieScreeningByIdAsync(id)
                ?? throw new MovieScreeningNotFoundException($"{id}");
            return mapper.Map<MovieScreeningDisplayDTO>(movieScreening);
        }

        public async Task<PagedResponseOffsetDTO<MovieScreeningDisplayDTO>> GetAllMovieScreenings(
            List<string> startLetter = null,
            string containsLetters = null,
            PagedResponseOffsetDTO<MovieScreeningDisplayDTO> cursorParameters = null
        )
        {
            var paginationParameters = mapper.Map<PagedResponseOffset<MovieScreening>>(
                cursorParameters
            );
            var movieRepertoirePageData = await unitOfWork.Screenings.GetMovieScreeningsAsync(
                startLetter,
                containsLetters,
                paginationParameters
            );

            return mapper.Map<PagedResponseOffsetDTO<MovieScreeningDisplayDTO>>(
                movieRepertoirePageData
            );
        }

        public async Task<MovieScreeningDisplayDTO> AddMovieScreening(
            MovieScreeningInputDTO movieScreeningInputDTO
        )
        {
            var movieScreening = mapper.Map<MovieScreening>(movieScreeningInputDTO);
            var movie =
                await unitOfWork.Movies.GetMovieByIdAsync(movieScreeningInputDTO.MovieId)
                ?? throw new MovieNotFoundException(
                    $"Movie with ID {movieScreeningInputDTO.MovieId} was not found!"
                );

            var checkIfScreeningTimeIsAvailable = await CanAddScreeningAsync(
                movie.Id,
                movieScreeningInputDTO.Date
            );
            if (checkIfScreeningTimeIsAvailable == false)
            {
                throw new TimeOverplapException($"{movieScreening.Date.TimeOfDay}");
            }
            movieScreening.Movie = movie;

            await unitOfWork.Screenings.AddCinemaRepertoireAsync(movieScreening);
            await unitOfWork.Complete();
            return mapper.Map<MovieScreeningDisplayDTO>(movieScreening);
        }

        public async Task<MovieScreeningDisplayDTO> UpdateMovieScreening(
            MovieScreeningUpdateDTO updateMovieScreening,
            long id
        )
        {
            var movieScreening =
                await unitOfWork.Screenings.GetMovieScreeningByIdAsync(id)
                ?? throw new MovieScreeningNotFoundException(
                    $"Movie screening with ID {id} was not found."
                );
            if (updateMovieScreening.Date != movieScreening.Date)
            {
                var reservations =
                    await unitOfWork.TicketReservations.GetReservationByScreeningIdAsync(id);
                reservations.ForEach(reservation => reservation.IsCancelled = true);
            }
            mapper.Map(updateMovieScreening, movieScreening);
            await unitOfWork.Complete();
            return mapper.Map<MovieScreeningDisplayDTO>(movieScreening);
        }

        public async Task DeleteMovieScreening(long id)
        {
            _ =
                await unitOfWork.Screenings.GetMovieScreeningByIdAsync(id)
                ?? throw new MovieScreeningNotFoundException(id.ToString());
            await unitOfWork.TicketReservations.CancelReservationsForMovieScreening(id);
            await unitOfWork.Screenings.DeleteMovieScreeningAsync(id);
            await unitOfWork.Complete();
        }

        private async Task<bool> CanAddScreeningAsync(long movieId, DateTime proposedScreeningTime)
        {
            var movie = await unitOfWork.Movies.GetMovieByIdAsync(movieId);
            var screenings = await unitOfWork.Screenings.GetScreeningsByMovieIdAsync(movieId);

            var screeningsOnSameDay = screenings
                .Where(s => s.Date.Date == proposedScreeningTime.Date && !s.IsDeleted)
                .ToList();

            var buffer = TimeSpan.FromMinutes(29);
            var movieDuration = TimeSpan.FromMinutes(movie.Duration);
            var totalBufferTime = movieDuration.Add(buffer);

            foreach (var screening in screeningsOnSameDay)
            {
                var bufferedScreeningStart = screening.Date.Subtract(totalBufferTime);
                var bufferedScreeningEnd = screening.Date.Add(movieDuration).Add(buffer);

                if (
                    proposedScreeningTime >= bufferedScreeningStart
                    && proposedScreeningTime <= bufferedScreeningEnd
                )
                {
                    return false;
                }
            }

            return true;
        }
    }
}
