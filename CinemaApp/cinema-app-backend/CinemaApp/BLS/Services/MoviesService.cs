using AutoMapper;
using DAL.Data.Utils;
using DAL.Interfaces;
using DOMAIN.DTOs;
using DOMAIN.Entities;
using DOMAIN.Exceptions;
using DOMAIN.MovieDTOs.DTOs;
using DOMAIN.ValueEntities;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace BLS.Services
{
    public class MoviesService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        private readonly IConfiguration configuration;
        private const string NoImagePlaceholderName = "No-Image-Placeholder.png";
        private readonly ILogger<MoviesService> logger;

        public MoviesService(
            IUnitOfWork unitOfWork,
            IMapper mapper,
            IConfiguration configuration,
            ILogger<MoviesService> logger
        )
        {
            this.logger = logger;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
            this.configuration = configuration;
        }

        public async Task<MoviesDisplayDTO> GetMovieById(long id)
        {
            var movie =
                await unitOfWork.Movies.GetMovieByIdAsync(id)
                ?? throw new MovieNotFoundException($"{id}");
            GetPosterImages(movie);
            return mapper.Map<MoviesDisplayDTO>(movie);
        }
        public async Task<Tuple<FileStream, string>> GetPosterImageAsync(long id)
        {
            var movie =
                await unitOfWork.Movies.GetMovieByIdAsync(id)
                ?? throw new MovieNotFoundException($"{id}");
            ;

            var posterImage = FilesUtil.Read(movie.PosterImage);
            if (posterImage == null)
            {
                var placeholderPath = Path.Combine(GetImagesFolderPath(), NoImagePlaceholderName);
                posterImage = FilesUtil.Read(placeholderPath);
            }

            return posterImage;
        }

        public async Task<List<CinemaRepertoireDisplayDTO>> GetCinemaRepertoireByDateAsync(
            DateTime dayOfScreening,
            string sort,
            string genre
        )
        {
            if (dayOfScreening.Date < DateTime.Now.Date || dayOfScreening.Date > DateTime.Now.Date.AddDays(7))
            {
                throw new EnteredReperoireDateHasPassed($"{dayOfScreening}");
            }


            var movies = await unitOfWork.Movies.GetMoviesWithScreeningsForDayAsync(
                dayOfScreening,
                sort,
                genre
            );

            GetPosterImages(movies.ToArray());
            var now = DateTime.UtcNow;
            movies.ForEach(m =>
                m.MovieScreenings = m
                    .MovieScreenings.Where(ms =>
                        ms.Date.Date == dayOfScreening.Date && !ms.IsDeleted && ms.Date > now
                    )
                    .ToList()
            );

            return mapper.Map<List<CinemaRepertoireDisplayDTO>>(movies);
        }


        public async Task<PagedResponseOffsetDTO<MoviesDisplayDTO>> GetAllMovies(
            List<string> startLetter = null,
            List<string> containsLetters = null,
            PagedResponseOffsetDTO<MoviesDisplayDTO> cursorParameters = null
        )
        {
            var paginationParameters = mapper.Map<PagedResponseOffset<Movie>>(cursorParameters);
            var moviesPageData = await unitOfWork.Movies.GetMoviesAsync(
                startLetter,
                containsLetters,
                paginationParameters
            );
            GetPosterImages(moviesPageData.Data.ToArray());
            return mapper.Map<PagedResponseOffsetDTO<MoviesDisplayDTO>>(moviesPageData);
        }

        public async Task<MoviesDisplayDTO> AddMovie(MovieInputDTO addMoviesDTO, IFormFile file)
        {
            var movie = mapper.Map<Movie>(addMoviesDTO);
            movie.Genres = await unitOfWork.Genres.GetGenresWithIds(addMoviesDTO.GenreIds);

            var placeholderImagePath = $"{GetImagesFolderPath()}/{NoImagePlaceholderName}";
            SetPosterImage(movie, file);
            if (movie.PosterImage == placeholderImagePath)
            {
                FilesUtil.Delete(movie.PosterImage);
            }
            await unitOfWork.Movies.AddMovieAsync(movie);
            await unitOfWork.Complete();
            return mapper.Map<MoviesDisplayDTO>(movie);
        }

        public async Task<MoviesDisplayDTO> UpdateMovie(
            MovieInputDTO addMovies,
            IFormFile file,
            long id
        )
        {
            var movie =
                await unitOfWork.Movies.GetMovieByIdAsync(id)
                ?? throw new MovieNotFoundException($"{id}");

            mapper.Map(addMovies, movie);
            movie.Genres = await unitOfWork.Genres.GetGenresWithIds(addMovies.GenreIds);

            if (file != null)
            {
                if (
                    !string.IsNullOrEmpty(movie.PosterImage)
                    && movie.PosterImage.Contains(file.FileName)
                )
                {
                    await unitOfWork.Complete();
                    return mapper.Map<MoviesDisplayDTO>(movie);
                }

                var placeholderImagePath = $"{GetImagesFolderPath()}/{NoImagePlaceholderName}";
                if (
                    !string.IsNullOrEmpty(movie.PosterImage)
                    && movie.PosterImage != placeholderImagePath
                )
                {
                    FilesUtil.Delete(movie.PosterImage);
                }
                SetPosterImage(movie, file);
            }
            await unitOfWork.Complete();
            return mapper.Map<MoviesDisplayDTO>(movie);
        }

        public async Task DeleteMovie(long id)
        {
            var movie =
                await unitOfWork.Movies.GetMovieByIdAsync(id)
                ?? throw new MovieNotFoundException($"{id}");
            if (movie.MovieScreenings != null)
            {
                movie.MovieScreenings.ForEach(ms =>
                    unitOfWork.Screenings.DeleteMovieScreeningAsync(ms.Id)
                );
                movie.MovieScreenings.ForEach(ms =>
                    unitOfWork.TicketReservations.CancelReservationsForMovieScreening(ms.Id)
                );
            }
            var placeholderImagePath = $"{GetImagesFolderPath()}/{NoImagePlaceholderName}";
            if (movie.PosterImage != placeholderImagePath && movie.PosterImage != null)
            {
                FilesUtil.Delete(movie.PosterImage);
            }
            await unitOfWork.Movies.DeleteMovieAsync(id);
            await unitOfWork.Complete();
            logger.LogDebug("", movie.Id);
        }

        public void GetPosterImages(params Movie[] movies)
        {
            string resourcesPath = configuration["BASE_URL"];
            string imagesPath = configuration["GET_IMAGES"];

            var imagePath = resourcesPath + imagesPath;
            foreach (var movie in movies)
            {
                movie.PosterImage = FilesUtil.GetPath(movie.PosterImage, imagePath);
            }
        }

        #region Private methods
        private static bool IsDateIn7DaysSpan(DateTime date)
        {
            var now = DateTime.Now.Date;
            var maxDate = now.AddDays(7);
            return date >= now && date <= maxDate;
        }

        private void SetPosterImage(Movie movie, IFormFile file)
        {
            if (file != null)
            {
                movie.PosterImage = FilesUtil.Save(file, GetImagesFolderPath());
            }
            else
            {
                movie.PosterImage = $"{GetImagesFolderPath()}/{NoImagePlaceholderName}";
            }
        }

    private string GetImagesFolderPath()
    {
        string resourcesPath = configuration["RESOURCES_PATH"];
        string imagesPath = configuration["IMAGES_PATH"];

        // if (imagesPath.StartsWith("\\") || imagesPath.StartsWith("/"))
        // {
        //     imagesPath = imagesPath.TrimStart('\\').TrimStart('/');
        // }

        return Path.Combine(resourcesPath, imagesPath);
    }


        #endregion
    }
}
