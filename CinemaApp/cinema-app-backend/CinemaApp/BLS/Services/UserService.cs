using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL.Interfaces;
using DOMAIN.DTOs;
using DOMAIN.Entities;

namespace BLS.Services
{
    public class UserService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public UserService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.mapper = mapper;
            this.unitOfWork = unitOfWork;
        }

        public async Task<UserDisplayDTO> GetByEmail(string email)
        {
            var user = await unitOfWork.UserRepository.GetUserByEmail(email);
            return mapper.Map<UserDisplayDTO>(user);
        }

        public async Task<UserDisplayDTO> GetById(long id)
        {
            var user = await unitOfWork.UserRepository.GetUserById(id);
            return mapper.Map<UserDisplayDTO>(user);
        }
    }
}
